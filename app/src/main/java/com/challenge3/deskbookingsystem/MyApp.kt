package com.challenge3.deskbookingsystem

import android.app.Application
import com.challenge3.deskbookingsystem.di.coreModule
import com.challenge3.deskbookingsystem.di.dbModule
import com.challenge3.deskbookingsystem.di.networkModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class MyApp : Application() {
    override fun onCreate() {
        super.onCreate()
        //koin
        startKoin {
            //context
            androidContext(this@MyApp)
            androidLogger()
            modules(
                dbModule,
                networkModule,
                coreModule
            )
        }
    }
}