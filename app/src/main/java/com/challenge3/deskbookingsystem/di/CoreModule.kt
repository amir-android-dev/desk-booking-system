package com.challenge3.deskbookingsystem.di

import com.challenge3.deskbookingsystem.repository.coreRepository.CoreRepository
import com.challenge3.deskbookingsystem.repository.coreRepository.CoreRepositoryImpl
import com.challenge3.deskbookingsystem.repository.paging.CommentsPagingSource
import com.challenge3.deskbookingsystem.ui.adminEdit.adapter.AdminDeskAdapter
import com.challenge3.deskbookingsystem.ui.adminEdit.adapter.EditEquipmentAdapter
import com.challenge3.deskbookingsystem.ui.adminEdit.viewModel.AdminEditDeskViewModel
import com.challenge3.deskbookingsystem.ui.adminEdit.viewModel.AdminEditOfficeViewModel
import com.challenge3.deskbookingsystem.ui.adminEdit.viewModel.AdminOfficeViewModel
import com.challenge3.deskbookingsystem.ui.admin_comments.AdminCommentsViewModel
import com.challenge3.deskbookingsystem.ui.admin_comments.CommentsAdapter
import com.challenge3.deskbookingsystem.ui.favorites.FavoritesViewModel
import com.challenge3.deskbookingsystem.ui.login.LoginViewModel
import com.challenge3.deskbookingsystem.ui.office.OfficeAdapter
import com.challenge3.deskbookingsystem.ui.office.OfficeViewModel
import com.challenge3.deskbookingsystem.ui.officeDesks.adapter.EquipmentAdapter
import com.challenge3.deskbookingsystem.ui.officeDesks.adapter.OfficeDesksAdapter
import com.challenge3.deskbookingsystem.ui.officeDesks.viewmodel.DeskDetailViewModel
import com.challenge3.deskbookingsystem.ui.officeDesks.viewmodel.OfficeDesksViewModel
import com.challenge3.deskbookingsystem.ui.profile.ProfileViewModel
import com.challenge3.deskbookingsystem.ui.register.RegisterViewModel
import com.challenge3.deskbookingsystem.ui.reservationsConfirm.ReservationConfirmViewModel
import com.challenge3.deskbookingsystem.ui.userBookings.CurrentBookingAdapter
import com.challenge3.deskbookingsystem.ui.userBookings.FixAdapter
import com.challenge3.deskbookingsystem.ui.userBookings.UserBookingViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.bind
import org.koin.dsl.module

val coreModule = module {
    single { CoreRepositoryImpl(localRepo = get(), remoteRepo = get()) } bind CoreRepository::class
    viewModel { LoginViewModel(coreRepo = get()) }
    viewModel { RegisterViewModel(coreRepo = get()) }
    viewModel { UserBookingViewModel(coreRepository = get()) }
    factory { params -> CurrentBookingAdapter(isPrevious = params.get()) }
    viewModel { OfficeViewModel(coreRepository = get()) }
    factory { OfficeAdapter() }
    viewModel { OfficeDesksViewModel(coreRepository = get()) }
    factory { OfficeDesksAdapter(context = get()) }
    viewModel { DeskDetailViewModel(coreRepository = get()) }
    factory { EquipmentAdapter() }
    viewModel { ProfileViewModel(coreRepo = get()) }
    viewModel { FavoritesViewModel(coreRepo = get()) }
    viewModel { ReservationConfirmViewModel(coreRepo = get()) }
    viewModel { AdminCommentsViewModel(coreRepo = get()) }
    factory { CommentsPagingSource(coreRepository = get()) }
    factory { CommentsAdapter() }
    viewModel {FavoritesViewModel(coreRepo = get())}
    viewModel { AdminOfficeViewModel( coreRepository = get() ) }
    viewModel { AdminEditOfficeViewModel( coreRepository = get()) }
    factory { AdminDeskAdapter() }
    factory { EditEquipmentAdapter() }
    viewModel { AdminEditDeskViewModel(coreRepository = get()) }
    factory { FixAdapter() }
}