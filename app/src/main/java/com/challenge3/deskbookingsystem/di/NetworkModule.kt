package com.challenge3.deskbookingsystem.di

import com.challenge3.deskbookingsystem.repository.remoteRepository.*
import com.challenge3.deskbookingsystem.repository.remoteRepository.AuthInterceptor
import com.challenge3.deskbookingsystem.ui.login.AuthTokenInterceptor
import com.challenge3.deskbookingsystem.utils.Constants
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.bind
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {
    single { provideGson() }
    single { AuthInterceptor(refreshTokenRepository = get(), context = get()) }
    single { provideInterceptor() }
    single { provideClient(interceptor = get(), authInterceptor = get()) }
    single { provideRetrofit(client = get(), gson = get()) }
    single { provideRetrofitAuth( gson = get(),get()) }
    single { RemoteRepositoryImpl(apiService = get()) } bind RemoteRepository::class
    single { RefreshTokenRepositoryImpl(refreshTokenApiService = get()) } bind RefreshTokenRepository::class
}

fun provideGson(): Gson = GsonBuilder().setLenient().create()

fun provideInterceptor() = HttpLoggingInterceptor().apply {
    level = HttpLoggingInterceptor.Level.BODY
}

fun provideClient(interceptor: HttpLoggingInterceptor, authInterceptor: AuthInterceptor) =
    OkHttpClient.Builder()
        .addInterceptor(interceptor).addInterceptor(authTokenInterceptor)
        .authenticator(authInterceptor)
        .build()

val authTokenInterceptor = AuthTokenInterceptor()

fun provideRetrofit(client: OkHttpClient, gson: Gson): ApiService =
    Retrofit.Builder()
        .baseUrl(Constants.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .client(client)
        .build()
        .create(ApiService::class.java)

fun provideRetrofitAuth(gson: Gson,interceptor: HttpLoggingInterceptor): RefreshTokenApiService =
    Retrofit.Builder()
        .baseUrl(Constants.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .client(OkHttpClient.Builder().addInterceptor(interceptor).build())
        .build()
        .create(RefreshTokenApiService::class.java)