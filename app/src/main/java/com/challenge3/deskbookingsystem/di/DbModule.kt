package com.challenge3.deskbookingsystem.di

import android.content.Context
import androidx.room.Room
import com.challenge3.deskbookingsystem.repository.localRepository.DeskBookingDb
import com.challenge3.deskbookingsystem.repository.localRepository.LocalRepository
import com.challenge3.deskbookingsystem.repository.localRepository.LocalRepositoryImpl
import com.challenge3.deskbookingsystem.utils.Constants
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.bind
import org.koin.dsl.module

val dbModule = module {
    single { provideDatabase(context = androidContext()) }
    single { provideDao(db = get()) }
    single { LocalRepositoryImpl(dao = get()) } bind LocalRepository::class
}

//database
fun provideDatabase(context: Context) =
    Room.databaseBuilder(context, DeskBookingDb::class.java, Constants.DESK_BOOKING_DATABASE)
        .allowMainThreadQueries()
        .fallbackToDestructiveMigration()
        .build()

//dao
fun provideDao(db: DeskBookingDb) = db.daoBookingDesk()

