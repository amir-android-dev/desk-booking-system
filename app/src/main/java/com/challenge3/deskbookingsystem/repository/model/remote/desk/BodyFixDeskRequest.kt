package com.challenge3.deskbookingsystem.repository.model.remote.desk

data class BodyFixDeskRequest(
    val desk: String
)
