package com.challenge3.deskbookingsystem.repository.model.remote.login

data class BodyLogin(
    val email: String,
    val password: String
)
