package com.challenge3.deskbookingsystem.repository.model.remote.desk

data class DeskWithBookings(
    val desk: Desk,
    var isAvailable: Boolean,
    var fixDesk: FixDesk?,
    val bookings: List<BookingOfDesk>
)
