package com.challenge3.deskbookingsystem.repository.model.remote.reservationsConfirm

data class ResponseApprovedFixDeskItem(
    val createdAt: String,
    val desk: DeskX,
    val id: String,
    val status: String,
    val updatedAt: String,
    val updatedBy: String,
    val user: UserX
)