package com.challenge3.deskbookingsystem.repository.model.remote.desk

data class BodyDesk(
    val label: String,
    val office: String,
    val equipment: List<String>
)
