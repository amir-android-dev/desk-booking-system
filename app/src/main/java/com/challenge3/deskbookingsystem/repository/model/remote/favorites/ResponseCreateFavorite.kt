package com.challenge3.deskbookingsystem.repository.model.remote.favorites

data class ResponseCreateFavorite(
    val createdAt: String,
    val desk: String,
    val id: String,
    val updatedAt: String,
    val user: String
)