package com.challenge3.deskbookingsystem.repository.remoteRepository

import android.content.Context
import com.challenge3.deskbookingsystem.repository.model.remote.login.BodyRefresh
import com.challenge3.deskbookingsystem.ui.login.AuthTokenInterceptor
import com.challenge3.deskbookingsystem.ui.login.Token
import com.challenge3.deskbookingsystem.utils.Constants
import com.challenge3.deskbookingsystem.utils.Resource
import kotlinx.coroutines.runBlocking
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route

class AuthInterceptor(private val refreshTokenRepository: RefreshTokenRepository, private val context: Context) :
    Authenticator {
    override fun authenticate(route: Route?, response: Response): Request? {
        if (AuthTokenInterceptor.refreshToken == null) return null
        try {
            return runBlocking {
                when (val resource =
                    refreshTokenRepository.refreshToken(BodyRefresh(AuthTokenInterceptor.refreshToken!!))) {
                    is Resource.Error -> {
                        null
                    }
                    is Resource.Success -> {
                        AuthTokenInterceptor.refreshToken = resource.result.refresh
                        AuthTokenInterceptor.token = resource.result.token
                        Token().saveToken(context, resource.result.token, resource.result.refresh)
                        response.request.newBuilder()
                            .header(
                                Constants.AUTHORIZATION,
                                "${Constants.BEARER} ${resource.result.token}"
                            )
                            .build()

                    }
                    Resource.SuccessNoBody -> {
                        null
                    }
                }
            }
        } catch (e: Throwable) {
            return null
        }
    }
}