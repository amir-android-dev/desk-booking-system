package com.challenge3.deskbookingsystem.repository.model.remote.desk

import com.challenge3.deskbookingsystem.repository.model.remote.office.Office
import com.google.gson.annotations.SerializedName

data class Desk (
        val label: String,
        val id: String,
        val nextBooking: String,
        @SerializedName("fixdesk")
        val fixDesk: FixDesk?,
        val type: String,
        val row: Int,
        val column: Int,
        val isUserFavourite: Boolean,
        val office: Office,
        val equipment: List<String>,
        val createdAt: String,
        val updatedAt: String,
)
