package com.challenge3.deskbookingsystem.repository.remoteRepository

import com.challenge3.deskbookingsystem.repository.model.remote.admin_comments.ResponseComments
import com.challenge3.deskbookingsystem.repository.model.remote.bookedDesk.ResponseBookedDesksByUserList.ResponseBookedDesksByUserListItem
import com.challenge3.deskbookingsystem.repository.model.remote.bookedDesk.ResponseDeleteDeskById
import com.challenge3.deskbookingsystem.repository.model.remote.booking.BodyBooking
import com.challenge3.deskbookingsystem.repository.model.remote.comment.ResponseComment
import com.challenge3.deskbookingsystem.repository.model.remote.comment.BodyComment
import com.challenge3.deskbookingsystem.repository.model.remote.desk.*
import com.challenge3.deskbookingsystem.repository.model.remote.favorites.BodyFavorites
import com.challenge3.deskbookingsystem.repository.model.remote.favorites.ResponseCreateFavorite
import com.challenge3.deskbookingsystem.repository.model.remote.favorites.ResponseFavorites
import com.challenge3.deskbookingsystem.repository.model.remote.fixDesks.ResponseBookedFixDesks.ResponseBookedFixDesksItem
import com.challenge3.deskbookingsystem.repository.model.remote.login.*
import com.challenge3.deskbookingsystem.repository.model.remote.office.BodyOffice
import com.challenge3.deskbookingsystem.repository.model.remote.office.Office
import com.challenge3.deskbookingsystem.repository.model.remote.register.BodyRegister
import com.challenge3.deskbookingsystem.repository.model.remote.register.ResponseRegister
import com.challenge3.deskbookingsystem.repository.model.remote.reservationsConfirm.BodyApprovedFixDesk
import com.challenge3.deskbookingsystem.repository.model.remote.reservationsConfirm.GetAllFixDesk
import com.challenge3.deskbookingsystem.repository.model.remote.reservationsConfirm.ReservationsErrorBody
import com.challenge3.deskbookingsystem.repository.model.remote.user.ResponseProfileOfCurrentUser
import com.challenge3.deskbookingsystem.utils.Resource
import com.challenge3.deskbookingsystem.utils.toResource
import com.google.gson.JsonObject
import retrofit2.Response
import java.net.SocketTimeoutException
import java.net.UnknownHostException

interface RemoteRepository {
    suspend fun login(body: BodyLogin): Resource<ResponseLogin>

    suspend fun registerUser(body: BodyRegister): Resource<ResponseRegister>

    suspend fun getProfileID(): Resource<ResponseProfile>

    suspend fun refreshToken(body: BodyRefresh): Resource<ResponseRefresh>

    suspend fun getAllOffices(): Resource<List<Office>>

    suspend fun getOffice(officeId: String): Resource<Office>

    suspend fun getAllDesks(): Resource<List<Desk>>

    suspend fun getAllBookingsOfDesk(deskId: String): Resource<List<ResponseDeskBooking>>

    //all booked desks by user
    suspend fun getAllBookingsUserMadeFromApi(userId: String): Resource<List<ResponseBookedDesksByUserListItem>?>

    //fixDesks
    suspend fun getAllFixDesks(userId: String): Resource<List<ResponseBookedFixDesksItem>?>

    //response of all comments a user made for admin
    suspend fun getComments(page: Int): Response<ResponseComments>

    suspend fun getProfileOfCurrentUser(): Resource<ResponseProfileOfCurrentUser>

    suspend fun getDesk(deskId: String): Resource<Desk>

    suspend fun createBooking(bodyBooking: BodyBooking): Resource<ResponseDeskBooking>

    suspend fun loadProfileUser(): Resource<ResponseProfileOfCurrentUser>

    suspend fun updateUserWithId(
        id: String,
        body: BodyRegister
    ): Resource<ResponseProfileOfCurrentUser>

    suspend fun deleteBookingById(id: String): Resource<ResponseDeleteDeskById?>
    suspend fun deleteFixDeskById(id: String): Resource<ResponseDeleteDeskById?>

    suspend fun getAllFavorites(id: String): Resource<ResponseFavorites>

    suspend fun deleteFavoriteWithID(id: String): Resource<String>

    //comment a desk
    suspend fun getAllFixDeskRequests(): Resource<GetAllFixDesk>

    suspend fun approveFixDeskRequest(body: BodyApprovedFixDesk): Resource<ReservationsErrorBody>

    suspend fun deleteFixDeskRequest(id: String): Resource<String>

    //comment a desk
    suspend fun postComment(bodyComment: BodyComment): Resource<ResponseComment>

    suspend fun createFavorite(desk: BodyFavorites): Resource<ResponseCreateFavorite>

    suspend fun createOffice(officeTitle: String): Resource<Office>

    suspend fun createDesk(deskLabel: String, officeId: String): Resource<NewDeskResponse>

    suspend fun editOffice(officeId: String, bodyOffice: BodyOffice): Resource<Office>

    suspend fun editDesk(deskId: String, bodyDesk: BodyDesk): Resource<Desk>

    suspend fun getPossibleEquipment(): Resource<JsonObject>
    suspend fun createFixDeskRequest(bodyFixDeskRequest: BodyFixDeskRequest): Resource<FixDeskRequest>
}

class RemoteRepositoryImpl(private val apiService: ApiService) :
    RemoteRepository {

    override suspend fun login(body: BodyLogin): Resource<ResponseLogin> {
        return sendRequestAndCatch {
            apiService.login(body).toResource()
        }
    }

    override suspend fun registerUser(body: BodyRegister): Resource<ResponseRegister> {
        return sendRequestAndCatch { apiService.registerUser(body).toResource() }
    }

    override suspend fun getProfileID(): Resource<ResponseProfile> {
        return sendRequestAndCatch { apiService.getProfileID().toResource() }
    }

    override suspend fun refreshToken(body: BodyRefresh): Resource<ResponseRefresh> {
        return sendRequestAndCatch { apiService.refreshToken(body).toResource() }
    }

    override suspend fun getAllOffices(): Resource<List<Office>> {
        return sendRequestAndCatch { apiService.getAllOffices().toResource() }
    }

    override suspend fun getOffice(officeId: String): Resource<Office> {
        return sendRequestAndCatch { apiService.getOffice(officeId).toResource() }
    }

    override suspend fun getAllDesks(): Resource<List<Desk>> {
        return sendRequestAndCatch { apiService.getAllDesks().toResource() }
    }

    override suspend fun getAllBookingsUserMadeFromApi(userId: String): Resource<List<ResponseBookedDesksByUserListItem>?> {
        return sendRequestAndCatch { apiService.getAllBookingsUserMade(userId).toResource() }
    }

    override suspend fun getAllFixDesks(userId: String): Resource<List<ResponseBookedFixDesksItem>?> {
        return sendRequestAndCatch { apiService.getAllBookingsFixDesks(userId).toResource() }
    }

    override suspend fun getComments(page: Int) = apiService.getComments(page)

    override suspend fun getProfileOfCurrentUser(): Resource<ResponseProfileOfCurrentUser> {
        return sendRequestAndCatch { apiService.getProfileOfCurrentUser().toResource() }
    }

    override suspend fun deleteBookingById(id: String): Resource<ResponseDeleteDeskById?> {
        return sendRequestAndCatch { apiService.deleteBookingWithId(id).toResource() }
    }

    override suspend fun deleteFixDeskById(id: String): Resource<ResponseDeleteDeskById?> {
        return sendRequestAndCatch { apiService.deleteFixDeskById(id).toResource() }
    }

    override suspend fun getAllFavorites(id: String): Resource<ResponseFavorites> {
        return sendRequestAndCatch { apiService.getAllFavorites(id).toResource() }
    }

    override suspend fun deleteFavoriteWithID(id: String): Resource<String> {
        return sendRequestAndCatch { apiService.deleteFavoriteWithID(id).toResource() }
    }

    override suspend fun postComment(bodyComment: BodyComment): Resource<ResponseComment> {
        return sendRequestAndCatch { apiService.postComment(bodyComment).toResource() }
    }

    override suspend fun createFavorite(desk: BodyFavorites): Resource<ResponseCreateFavorite> {
        return sendRequestAndCatch { apiService.createFavorite(desk).toResource() }
    }

    override suspend fun getAllFixDeskRequests(): Resource<GetAllFixDesk> {
        return sendRequestAndCatch { apiService.getAllFixDeskRequests().toResource() }
    }

    override suspend fun approveFixDeskRequest(body: BodyApprovedFixDesk): Resource<ReservationsErrorBody> {
        return sendRequestAndCatch { apiService.approveFixDeskRequest(body).toResource() }
    }

    override suspend fun deleteFixDeskRequest(id: String): Resource<String> {
        return sendRequestAndCatch { apiService.deleteFixDeskRequest(id).toResource() }
    }

    override suspend fun createOffice(officeTitle: String): Resource<Office> {
        return sendRequestAndCatch {
            apiService.createOffice(BodyOffice(officeTitle, 0, 0)).toResource()
        }
    }

    override suspend fun createDesk(
        deskLabel: String,
        officeId: String
    ): Resource<NewDeskResponse> {
        return sendRequestAndCatch {
            apiService.createDesk(BodyDesk(deskLabel, officeId, listOf())).toResource()
        }
    }

    override suspend fun editOffice(officeId: String, bodyOffice: BodyOffice): Resource<Office> {
        return sendRequestAndCatch { apiService.putOffice(officeId, bodyOffice).toResource() }
    }

    override suspend fun editDesk(deskId: String, bodyDesk: BodyDesk): Resource<Desk> {
        return sendRequestAndCatch { apiService.putDesk(deskId, bodyDesk).toResource() }
    }

    override suspend fun getPossibleEquipment(): Resource<JsonObject> {
        return sendRequestAndCatch { apiService.getEquipment().toResource() }
    }

    override suspend fun createFixDeskRequest(bodyFixDeskRequest: BodyFixDeskRequest): Resource<FixDeskRequest> {
        return sendRequestAndCatch {
            apiService.createFixDeskRequest(bodyFixDeskRequest).toResource()
        }
    }

    override suspend fun loadProfileUser(): Resource<ResponseProfileOfCurrentUser> {
        return sendRequestAndCatch { apiService.loadProfileUser().toResource() }
    }

    override suspend fun getAllBookingsOfDesk(deskId: String): Resource<List<ResponseDeskBooking>> {
        return sendRequestAndCatch { apiService.getAllBookingsOfDesk(deskId).toResource() }
    }

    override suspend fun updateUserWithId(
        id: String,
        body: BodyRegister
    ): Resource<ResponseProfileOfCurrentUser> {
        return sendRequestAndCatch { apiService.updateUserWithID(id, body).toResource() }
    }


    override suspend fun getDesk(deskId: String): Resource<Desk> {
        return sendRequestAndCatch { apiService.getDesk(deskId).toResource() }
    }

    override suspend fun createBooking(bodyBooking: BodyBooking): Resource<ResponseDeskBooking> {
        return sendRequestAndCatch { apiService.createBooking(bodyBooking).toResource() }
    }


    private suspend fun <T> sendRequestAndCatch(request: suspend () -> Resource<T>): Resource<T> {
        return try {
            return request()
        } catch (e: SocketTimeoutException) {
            Resource.Error(RemoteError.NoConnectionError)
        } catch (e: UnknownHostException) {
            Resource.Error(RemoteError.NoConnectionError)
        } catch (e: Throwable) {
            Resource.Error(RemoteError.UnknownError)
        }
    }

    sealed class RemoteError : Throwable() {
        object UnauthorizedError : RemoteError()
        object BadRequestError : RemoteError()
        object UnknownError : RemoteError()
        object NoConnectionError : RemoteError()
        object NotFoundError : RemoteError()
        object ConflictError : RemoteError()
    }
}