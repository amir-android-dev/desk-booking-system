package com.challenge3.deskbookingsystem.repository.model.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.challenge3.deskbookingsystem.utils.Constants

@Entity(tableName = Constants.BOOKED_DESK_TABLE)
data class BookedDeskDbModel(
    @PrimaryKey(autoGenerate = false)
    val id: String = "0", // 51f62a6f-c0e1-4dad-867f-6d09136970ff
    val dateEnd: String? = "0", // 2023-02-24T00:00:00.000Z
    val dateStart: String? = "0", // 2023-02-24T00:00:00.000Z
    val officeName: String? = "0", // berlin
    val type: String? = "0", // flex
    val deskLabel: String? = "0", //Du
    val equipments: List<String?>?,// [HDMI, MONITOR]
    val deskId: String?

)