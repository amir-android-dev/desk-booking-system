package com.challenge3.deskbookingsystem.repository.model.remote.admin_comments


import com.challenge3.deskbookingsystem.repository.model.remote.admin_comments.ResponseComments.ResponseCommentsItem
import com.google.gson.annotations.SerializedName

class ResponseComments : ArrayList<ResponseCommentsItem>(){
    data class ResponseCommentsItem(
        @SerializedName("comment")
        val comment: String?, // ipsum2
        @SerializedName("commentedAt")
        val commentedAt: String?, // 2023-02-28T18:08:47.448Z
        @SerializedName("desk")
        val desk: Desk?,
        @SerializedName("id")
        val id: String?, // a9ea3a2c-ad7f-4061-8eb8-387234899236
        @SerializedName("updatedAt")
        val updatedAt: String?, // 2023-02-28T18:08:47.448Z
        @SerializedName("user")
        val user: User?
    ) {
        data class Desk(
            @SerializedName("column")
            val column: Int?, // 0
            @SerializedName("createdAt")
            val createdAt: String?, // 2023-02-28T08:21:52.048Z
            @SerializedName("equipment")
            val equipment: List<String?>?,
            @SerializedName("id")
            val id: String?, // 90105935-b614-4c3e-846f-be334564a92e
            @SerializedName("label")
            val label: String?, // VolkiOne
            @SerializedName("row")
            val row: Int?, // 1
            @SerializedName("type")
            val type: String?, // flex
            @SerializedName("updatedAt")
            val updatedAt: String? // 2023-02-28T08:21:52.048Z
        )
    
        data class User(
            @SerializedName("createdAt")
            val createdAt: String?, // 2023-02-16T09:18:21.493Z
            @SerializedName("department")
            val department: String?, // DarwinsLab
            @SerializedName("email")
            val email: String?, // admin3@csaw.at
            @SerializedName("firstname")
            val firstname: String?, // Admin
            @SerializedName("id")
            val id: String?, // 58aaf4f5-5ba8-4827-af85-58d4a69607d2
            @SerializedName("isAdmin")
            val isAdmin: Boolean?, // true
            @SerializedName("lastname")
            val lastname: String?, // User1
            @SerializedName("updatedAt")
            val updatedAt: String? // 2023-02-28T19:34:21.533Z
        )
    }
}