package com.challenge3.deskbookingsystem.repository.model.remote.comment


import com.google.gson.annotations.SerializedName

data class ResponseComment(
    @SerializedName("comment")
    val comment: String?, // labore do eu ullamco occaecat
    @SerializedName("commentedAt")
    val commentedAt: String?, // 2023-02-27T16:12:17.520Z
    @SerializedName("desk")
    val desk: String?, // a300b596-43e4-4697-838a-9346a770a15f
    @SerializedName("id")
    val id: String?, // e91d358a-0c49-4572-ac05-7ea9d8240d12
    @SerializedName("updatedAt")
    val updatedAt: String?, // 2023-02-27T16:12:17.520Z
    @SerializedName("user")
    val user: String? // 58aaf4f5-5ba8-4827-af85-58d4a69607d2
)