package com.challenge3.deskbookingsystem.repository.localRepository

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.challenge3.deskbookingsystem.repository.model.db.BookedDeskDbModel
import com.challenge3.deskbookingsystem.repository.model.db.BookedFixDesksDbModel
import com.challenge3.deskbookingsystem.repository.model.db.StringListTypeConverter

@TypeConverters(StringListTypeConverter::class)
@Database(
    entities = [BookedDeskDbModel::class, BookedFixDesksDbModel::class],
    version = 1,
    exportSchema = false
)
abstract class DeskBookingDb : RoomDatabase() {

    abstract fun daoBookingDesk(): DeskBookingDao
}