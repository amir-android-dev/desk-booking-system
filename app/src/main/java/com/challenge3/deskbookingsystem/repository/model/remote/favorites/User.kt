package com.challenge3.deskbookingsystem.repository.model.remote.favorites

data class User(
    val createdAt: String,
    val department: String,
    val email: String,
    val firstname: String,
    val id: String,
    val isAdmin: Boolean,
    val lastname: String,
    val updatedAt: String
)