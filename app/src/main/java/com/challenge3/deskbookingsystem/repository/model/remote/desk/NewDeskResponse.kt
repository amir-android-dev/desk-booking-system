package com.challenge3.deskbookingsystem.repository.model.remote.desk

import com.challenge3.deskbookingsystem.repository.model.remote.office.Office
import com.google.gson.annotations.SerializedName

//Desk response of creating desk
data class NewDeskResponse (
        val label: String,
        val id: String,
        @SerializedName("fixdesk")
        val fixDesk: String?,
        val type: String,
        val office: String,
        val equipment: List<String>,
        val createdAt: String,
        val updatedAt: String,
)
