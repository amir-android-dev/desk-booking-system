package com.challenge3.deskbookingsystem.repository.model.remote.office

import com.challenge3.deskbookingsystem.repository.model.remote.desk.Desk

data class Office (
    val name: String,
    val columns: Int,
    val rows: Int,
    val id: String,
    val map: String,
    val createdAt: String,
    val updatedAt: String,
    val desks: List<Desk>?
    )
