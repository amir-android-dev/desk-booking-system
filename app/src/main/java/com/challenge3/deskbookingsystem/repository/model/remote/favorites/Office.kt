package com.challenge3.deskbookingsystem.repository.model.remote.favorites

data class Office(
    val columns: Int,
    val createdAt: String,
    val id: String,
    val map: String,
    val name: String,
    val rows: Int,
    val updatedAt: String
)