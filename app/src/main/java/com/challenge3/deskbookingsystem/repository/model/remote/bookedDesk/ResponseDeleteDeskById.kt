package com.challenge3.deskbookingsystem.repository.model.remote.bookedDesk


import com.google.gson.annotations.SerializedName

data class ResponseDeleteDeskById(
    @SerializedName("error")
    val error: String?, // Bad Request
    @SerializedName("message")
    val message: String?, // Validation failed (uuid  is expected)
    @SerializedName("statusCode")
    val statusCode: Int // 400
)