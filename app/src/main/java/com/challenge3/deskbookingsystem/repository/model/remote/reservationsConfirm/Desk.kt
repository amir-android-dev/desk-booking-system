package com.challenge3.deskbookingsystem.repository.model.remote.reservationsConfirm

data class Desk(
    val column: Int,
    val createdAt: String,
    val equipment: List<String>,
    val id: String,
    val label: String,
    val office: Office,
    val row: Int,
    val type: String,
    val updatedAt: String
)