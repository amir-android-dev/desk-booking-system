package com.challenge3.deskbookingsystem.repository.model.remote.fixDesks


import com.challenge3.deskbookingsystem.repository.model.remote.fixDesks.ResponseBookedFixDesks.ResponseBookedFixDesksItem
import com.google.gson.annotations.SerializedName

class ResponseBookedFixDesks : ArrayList<ResponseBookedFixDesksItem>(){
    data class ResponseBookedFixDesksItem(
        @SerializedName("createdAt")
        val createdAt: String?, // 2023-03-10T11:11:56.373Z
        @SerializedName("desk")
        val desk: Desk?,
        @SerializedName("id")
        val id: String?, // 44a79a62-f805-4a80-bf87-c7c4d8a24e15
        @SerializedName("status")
        val status: String?, // approved
        @SerializedName("updatedAt")
        val updatedAt: String?, // 2023-03-10T11:12:03.081Z
        @SerializedName("updatedBy")
        val updatedBy: UpdatedBy?,
        @SerializedName("user")
        val user: User?
    ) {
        data class Desk(
            @SerializedName("column")
            val column: Int?, // 0
            @SerializedName("createdAt")
            val createdAt: String?, // 2023-03-09T16:41:36.869Z
            @SerializedName("equipment")
            val equipment: List<String?>?,
            @SerializedName("id")
            val id: String?, // 697eae4f-888e-4d50-9a76-a73be50097c2
            @SerializedName("label")
            val label: String?, // Tisch-1
            @SerializedName("office")
            val office: Office?,
            @SerializedName("row")
            val row: Int?, // 0
            @SerializedName("type")
            val type: String?, // flex
            @SerializedName("updatedAt")
            val updatedAt: String? // 2023-03-10T08:25:58.707Z
        ) {
            data class Office(
                @SerializedName("columns")
                val columns: Int?, // 6
                @SerializedName("createdAt")
                val createdAt: String?, // 2023-03-09T16:41:19.093Z
                @SerializedName("id")
                val id: String?, // b73aac6d-9ed0-41c9-96e0-a1612fee5ae7
                @SerializedName("map")
                val map: String?, // maps/b73aac6d-9ed0-41c9-96e0-a1612fee5ae7.jpg
                @SerializedName("name")
                val name: String?, // Buuuuuro
                @SerializedName("rows")
                val rows: Int?, // 20
                @SerializedName("updatedAt")
                val updatedAt: String? // 2023-03-09T16:41:36.894Z
            )
        }
    
        data class UpdatedBy(
            @SerializedName("createdAt")
            val createdAt: String?, // 2023-02-16T09:18:21.493Z
            @SerializedName("department")
            val department: String?, // WebundSoehne
            @SerializedName("email")
            val email: String?, // admin3@csaw.at
            @SerializedName("firstname")
            val firstname: String?, // Admin6
            @SerializedName("id")
            val id: String?, // 58aaf4f5-5ba8-4827-af85-58d4a69607d2
            @SerializedName("isAdmin")
            val isAdmin: Boolean?, // true
            @SerializedName("lastname")
            val lastname: String?, // User
            @SerializedName("updatedAt")
            val updatedAt: String? // 2023-03-10T11:32:18.827Z
        )
    
        data class User(
            @SerializedName("createdAt")
            val createdAt: String?, // 2023-02-16T09:18:21.493Z
            @SerializedName("department")
            val department: String?, // WebundSoehne
            @SerializedName("email")
            val email: String?, // admin3@csaw.at
            @SerializedName("firstname")
            val firstname: String?, // Admin6
            @SerializedName("id")
            val id: String?, // 58aaf4f5-5ba8-4827-af85-58d4a69607d2
            @SerializedName("isAdmin")
            val isAdmin: Boolean?, // true
            @SerializedName("lastname")
            val lastname: String?, // User
            @SerializedName("updatedAt")
            val updatedAt: String? // 2023-03-10T11:32:18.827Z
        )
    }
}