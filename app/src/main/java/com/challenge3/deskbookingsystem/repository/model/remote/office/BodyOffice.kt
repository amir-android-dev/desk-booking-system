package com.challenge3.deskbookingsystem.repository.model.remote.office

data class BodyOffice(
    var name: String,
    var columns: Int,
    var rows: Int
)
