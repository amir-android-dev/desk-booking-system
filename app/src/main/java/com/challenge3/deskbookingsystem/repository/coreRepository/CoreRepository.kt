package com.challenge3.deskbookingsystem.repository.coreRepository

import com.challenge3.deskbookingsystem.repository.localRepository.LocalRepository
import com.challenge3.deskbookingsystem.repository.model.db.BookedDeskDbModel
import com.challenge3.deskbookingsystem.repository.model.db.BookedFixDesksDbModel
import com.challenge3.deskbookingsystem.repository.model.remote.admin_comments.ResponseComments
import com.challenge3.deskbookingsystem.repository.model.remote.bookedDesk.ResponseDeleteDeskById
import com.challenge3.deskbookingsystem.repository.model.remote.comment.BodyComment
import com.challenge3.deskbookingsystem.repository.model.remote.booking.BodyBooking
import com.challenge3.deskbookingsystem.repository.model.remote.comment.ResponseComment
import com.challenge3.deskbookingsystem.repository.model.remote.desk.*
import com.challenge3.deskbookingsystem.repository.model.remote.favorites.BodyFavorites
import com.challenge3.deskbookingsystem.repository.model.remote.favorites.ResponseCreateFavorite
import com.challenge3.deskbookingsystem.repository.model.remote.favorites.ResponseFavorites
import com.challenge3.deskbookingsystem.repository.model.remote.login.*
import com.challenge3.deskbookingsystem.repository.model.remote.office.BodyOffice
import com.challenge3.deskbookingsystem.repository.model.remote.office.Office
import com.challenge3.deskbookingsystem.repository.model.remote.register.BodyRegister
import com.challenge3.deskbookingsystem.repository.model.remote.register.ResponseRegister
import com.challenge3.deskbookingsystem.repository.model.remote.reservationsConfirm.BodyApprovedFixDesk
import com.challenge3.deskbookingsystem.repository.model.remote.reservationsConfirm.ReservationsErrorBody
import com.challenge3.deskbookingsystem.repository.model.remote.reservationsConfirm.GetAllFixDesk
import com.challenge3.deskbookingsystem.repository.model.remote.user.ResponseProfileOfCurrentUser
import com.challenge3.deskbookingsystem.repository.remoteRepository.RemoteRepository
import com.challenge3.deskbookingsystem.repository.remoteRepository.RemoteRepositoryImpl.*
import com.challenge3.deskbookingsystem.utils.Constants.NO_CONNECTION
import com.challenge3.deskbookingsystem.utils.Constants.SUCCESS
import com.challenge3.deskbookingsystem.utils.Resource
import com.challenge3.deskbookingsystem.utils.messageId
import com.google.gson.JsonObject
import retrofit2.Response

interface CoreRepository {
    suspend fun login(body: BodyLogin): Resource<ResponseLogin>

    suspend fun registerUser(body: BodyRegister): Resource<ResponseRegister>

    suspend fun getProfileID(): Resource<ResponseProfile>

    suspend fun refreshToken(body: BodyRefresh): Resource<ResponseRefresh>

    //all booked desks by user load from remote and insert in db
    suspend fun loadAndInsertBookingsDesk(): Int
    suspend fun loadAndInsertFixDesk(): Int

    //all booked flex desks of a user
    fun getAllBookedDesks(): List<BookedDeskDbModel>

    //fix desks
    fun getAllFixDesks(): List<BookedFixDesksDbModel>

    suspend fun getComments(page: Int): Response<ResponseComments>

    suspend fun getProfileOfCurrentUser(): Resource<ResponseProfileOfCurrentUser>

    suspend fun loadProfileUser(): Resource<ResponseProfileOfCurrentUser>

    suspend fun updateUserWithID(
        id: String,
        body: BodyRegister
    ): Resource<ResponseProfileOfCurrentUser>

    suspend fun deleteBookingWithId(id: String): Resource<ResponseDeleteDeskById?>
    suspend fun deleteFixDeskById(id: String): Resource<ResponseDeleteDeskById?>

    suspend fun getAllFavorites(id: String): Resource<ResponseFavorites>

    //comment a desk
    suspend fun postComment(bodyComment: BodyComment): Resource<ResponseComment>

    suspend fun getAllOffices(): Resource<List<Office>>

    suspend fun getOffice(officeId: String): Resource<Office>

    suspend fun getAllBookingsOfDesk(deskId: String): Resource<List<ResponseDeskBooking>>

    suspend fun getDesk(deskId: String): Resource<Desk>

    suspend fun createBooking(bodyBooking: BodyBooking): Resource<ResponseDeskBooking>

    suspend fun createFavorite(desk: BodyFavorites): Resource<ResponseCreateFavorite>

    suspend fun deleteFavoriteWithID(id: String): Resource<String>

    suspend fun getAllFixDeskRequests(): Resource<GetAllFixDesk>

    suspend fun approveFixDeskRequest(body: BodyApprovedFixDesk): Resource<ReservationsErrorBody>

    suspend fun deleteFixDeskRequest(id: String): Resource<String>

    suspend fun createOffice(officeTitle: String): Resource<Office>

    suspend fun createDesk(deskLabel: String, officeId: String): Resource<NewDeskResponse>

    suspend fun editOffice(officeId: String, bodyOffice: BodyOffice): Resource<Office>

    suspend fun editDesk(deskId: String, bodyDesk: BodyDesk): Resource<Desk>

    suspend fun getPossibleEquipment(): Resource<JsonObject>
    suspend fun createFixDesk(bodyFixDeskRequest: BodyFixDeskRequest): Resource<FixDeskRequest>
}

class CoreRepositoryImpl(
    private val localRepo: LocalRepository,
    private val remoteRepo: RemoteRepository
) :
    CoreRepository {
    // login
    override suspend fun login(body: BodyLogin): Resource<ResponseLogin> {
        return remoteRepo.login(body)
    }

    // register
    override suspend fun registerUser(body: BodyRegister): Resource<ResponseRegister> {
        return remoteRepo.registerUser(body)
    }

    override suspend fun getProfileID(): Resource<ResponseProfile> {
        return remoteRepo.getProfileID()
    }

    override suspend fun refreshToken(body: BodyRefresh): Resource<ResponseRefresh> {
        return remoteRepo.refreshToken(body)
    }

    override suspend fun loadProfileUser(): Resource<ResponseProfileOfCurrentUser> {
        return remoteRepo.loadProfileUser()
    }

    override suspend fun updateUserWithID(
        id: String,
        body: BodyRegister
    ): Resource<ResponseProfileOfCurrentUser> {
        return remoteRepo.updateUserWithId(id, body)
    }

    override suspend fun loadAndInsertBookingsDesk(): Int {
        //current logged user
        when (val responseCurrentUser = remoteRepo.getProfileOfCurrentUser()) {
            is Resource.Error -> {
                if (responseCurrentUser.throwable is RemoteError.NoConnectionError) {
                    return NO_CONNECTION
                }
                return (responseCurrentUser.throwable as RemoteError).messageId()
            }
            is Resource.Success -> {
                //desks of current user
                val response =
                    remoteRepo.getAllBookingsUserMadeFromApi(responseCurrentUser.result.id!!)
                when (response) {
                    is Resource.Error -> {
                        return (response.throwable as RemoteError).messageId()
                    }
                    is Resource.Success -> {
                        localRepo.deleteAllDesks()
                        response.result!!.map { apiResponse ->
                            localRepo.insertBookedDesk(
                                BookedDeskDbModel(
                                    apiResponse.id!!,
                                    apiResponse.dateEnd,
                                    apiResponse.dateStart,
                                    apiResponse.desk?.office?.name,
                                    apiResponse.desk?.type,
                                    apiResponse.desk?.label,
                                    apiResponse.desk?.equipment,
                                    apiResponse.desk?.id
                                )
                            )
                        }
                        return SUCCESS
                    }
                    Resource.SuccessNoBody -> {
                        return SUCCESS
                    }
                }
            }
            Resource.SuccessNoBody -> {
                return SUCCESS
            }
        }
    }

    override suspend fun loadAndInsertFixDesk(): Int {
        //current logged user
        when (val responseCurrentUser = remoteRepo.getProfileOfCurrentUser()) {
            is Resource.Error -> {
                if (responseCurrentUser.throwable is RemoteError.NoConnectionError) {
                    return NO_CONNECTION
                }
                return (responseCurrentUser.throwable as RemoteError).messageId()
            }
            is Resource.Success -> {
                //desks of current user
                val response =
                    remoteRepo.getAllFixDesks(responseCurrentUser.result.id!!)
                when (response) {
                    is Resource.Error -> {
                        return (response.throwable as RemoteError).messageId()
                    }
                    is Resource.Success -> {
                        localRepo.deleteAllFixDesks()
                        response.result!!.map { apiResponse ->
                            localRepo.insertFixdDesk(
                                BookedFixDesksDbModel(
                                    id = apiResponse.id!!,
                                    status = apiResponse.status,
                                    label = apiResponse.desk?.label,
                                    type = "Fix",
                                    office_name = apiResponse.desk?.office?.name
                                )
                            )
                        }
                        return SUCCESS
                    }
                    Resource.SuccessNoBody -> {
                        return SUCCESS
                    }
                }
            }
            Resource.SuccessNoBody -> {
                return SUCCESS
            }
        }
    }

    override fun getAllBookedDesks() = localRepo.getAllBookedDesks()
    override fun getAllFixDesks() = localRepo.getAllFixDesks()

    override suspend fun getComments(page: Int) = remoteRepo.getComments(page)

    override suspend fun getProfileOfCurrentUser(): Resource<ResponseProfileOfCurrentUser> {
        return remoteRepo.getProfileOfCurrentUser()
    }

    override suspend fun deleteBookingWithId(id: String): Resource<ResponseDeleteDeskById?> {
        val response = remoteRepo.deleteBookingById(id)
        if (response is Resource.Success) {
            localRepo.deleteBookedDeskById(id)
        }
        return response
    }

    override suspend fun deleteFixDeskById(id: String): Resource<ResponseDeleteDeskById?> {
        val response = remoteRepo.deleteFixDeskById(id)
        if (response is Resource.Success) {
            localRepo.deleteFixDeskById(id)
        }
        return response
    }

    //to comment previous desk
    override suspend fun postComment(bodyComment: BodyComment): Resource<ResponseComment> {
        return remoteRepo.postComment(bodyComment)
    }


    override suspend fun getAllFavorites(id: String): Resource<ResponseFavorites> {
        remoteRepo.getAllFavorites(id)
        return remoteRepo.getAllFavorites(id)
    }

    override suspend fun getAllOffices(): Resource<List<Office>> {
        return remoteRepo.getAllOffices()
    }

    override suspend fun getOffice(officeId: String): Resource<Office> {
        return remoteRepo.getOffice(officeId)
    }

    override suspend fun getAllBookingsOfDesk(deskId: String): Resource<List<ResponseDeskBooking>> {
        return remoteRepo.getAllBookingsOfDesk(deskId)
    }

    override suspend fun getDesk(deskId: String): Resource<Desk> {
        return remoteRepo.getDesk(deskId)
    }

    override suspend fun createBooking(bodyBooking: BodyBooking): Resource<ResponseDeskBooking> {
        return remoteRepo.createBooking(bodyBooking)
    }

    override suspend fun createFavorite(desk: BodyFavorites): Resource<ResponseCreateFavorite> {
        return remoteRepo.createFavorite(desk)
    }

    override suspend fun deleteFavoriteWithID(id: String): Resource<String> {
        return remoteRepo.deleteFavoriteWithID(id)
    }

    override suspend fun getAllFixDeskRequests(): Resource<GetAllFixDesk> {
        return remoteRepo.getAllFixDeskRequests()
    }

    override suspend fun approveFixDeskRequest(body: BodyApprovedFixDesk): Resource<ReservationsErrorBody> {
        return remoteRepo.approveFixDeskRequest(body)
    }

    override suspend fun deleteFixDeskRequest(id: String): Resource<String> {
        return remoteRepo.deleteFixDeskRequest(id)
    }

    override suspend fun createOffice(officeTitle: String): Resource<Office> {
        return remoteRepo.createOffice(officeTitle)
    }

    override suspend fun createDesk(
        deskLabel: String,
        officeId: String
    ): Resource<NewDeskResponse> {
        return remoteRepo.createDesk(deskLabel, officeId)
    }

    override suspend fun editOffice(officeId: String, bodyOffice: BodyOffice): Resource<Office> {
        return remoteRepo.editOffice(officeId, bodyOffice)
    }

    override suspend fun editDesk(deskId: String, bodyDesk: BodyDesk): Resource<Desk> {
        return remoteRepo.editDesk(deskId, bodyDesk)
    }

    override suspend fun getPossibleEquipment(): Resource<JsonObject> {
        return remoteRepo.getPossibleEquipment()
    }

    override suspend fun createFixDesk(bodyFixDeskRequest: BodyFixDeskRequest): Resource<FixDeskRequest> {
        return remoteRepo.createFixDeskRequest(bodyFixDeskRequest)
    }
}