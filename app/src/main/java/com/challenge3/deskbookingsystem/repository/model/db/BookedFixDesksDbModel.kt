package com.challenge3.deskbookingsystem.repository.model.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.challenge3.deskbookingsystem.utils.Constants

@Entity(Constants.BOOKED_FIX_DESK_TABLE)
data class BookedFixDesksDbModel(
    @PrimaryKey(autoGenerate = false)
    val id: String = "0",
    val status: String?,
    val label: String?,
    val type: String,
    val office_name: String?
)
