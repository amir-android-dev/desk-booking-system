package com.challenge3.deskbookingsystem.repository.model.remote.bookedDesk


import com.challenge3.deskbookingsystem.repository.model.remote.bookedDesk.ResponseBookedDesksByUserList.ResponseBookedDesksByUserListItem
import com.google.gson.annotations.SerializedName

class ResponseBookedDesksByUserList : ArrayList<ResponseBookedDesksByUserListItem>() {
    data class ResponseBookedDesksByUserListItem(
        @SerializedName("bookedAt")
        val bookedAt: String?, // 2023-02-22T19:11:52.286Z
        @SerializedName("dateEnd")
        val dateEnd: String?, // 2023-02-24T00:00:00.000Z
        @SerializedName("dateStart")
        val dateStart: String?, // 2023-02-22T00:00:00.000Z
        @SerializedName("desk")
        val desk: Desk?,
        @SerializedName("id")
        val id: String?, // 51f62a6f-c0e1-4dad-867f-6d09136970ff
        @SerializedName("user")
        val user: User?
    ) {
        data class Desk(
            @SerializedName("column")
            val column: Int?, // 0
            @SerializedName("createdAt")
            val createdAt: String?, // 2023-02-20T18:34:37.084Z
            @SerializedName("equipment")
            val equipment: List<String?>?,
            @SerializedName("id")
            val id: String?, // 8a1ab4ab-04c6-4a6e-a903-a30aecc7d1a5
            @SerializedName("label")
            val label: String?, // 1
            @SerializedName("office")
            val office: Office?,
            @SerializedName("row")
            val row: Int?, // 0
            @SerializedName("type")
            val type: String?, // flex
            @SerializedName("updatedAt")
            val updatedAt: String? // 2023-02-20T18:34:37.084Z
        ) {
            data class Office(
                @SerializedName("columns")
                val columns: Int?, // 5
                @SerializedName("createdAt")
                val createdAt: String?, // 2023-02-20T15:13:51.955Z
                @SerializedName("id")
                val id: String?, // 6400e936-cb89-41c6-afaa-e9c95e782bc6
                @SerializedName("map")
                val map: String?, // maps/6400e936-cb89-41c6-afaa-e9c95e782bc6.jpg
                @SerializedName("name")
                val name: String?, // berlin
                @SerializedName("rows")
                val rows: Int?, // 4
                @SerializedName("updatedAt")
                val updatedAt: String? // 2023-02-22T18:47:04.943Z
            )
        }

        data class User(
            @SerializedName("createdAt")
            val createdAt: String?, // 2023-02-22T08:21:06.964Z
            @SerializedName("department")
            val department: String?, // DarwinsLab
            @SerializedName("email")
            val email: String?, // test5@email.com
            @SerializedName("firstname")
            val firstname: String?, // occaecat laborum ut
            @SerializedName("id")
            val id: String?, // 5231aabc-8b98-4112-b4ba-b2a063412637
            @SerializedName("isAdmin")
            val isAdmin: Boolean?, // false
            @SerializedName("lastname")
            val lastname: String?, // aliqua xxxxxx
            @SerializedName("updatedAt")
            val updatedAt: String? // 2023-02-22T08:30:19.889Z
        )
    }
}