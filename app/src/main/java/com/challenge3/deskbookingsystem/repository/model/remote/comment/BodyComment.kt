package com.challenge3.deskbookingsystem.repository.model.remote.comment


import com.google.gson.annotations.SerializedName

data class BodyComment(
    @SerializedName("comment")
    val comment: String?, // labore do eu ullamco occaecat
    @SerializedName("desk")
    val desk: String? // a300b596-43e4-4697-838a-9346a770a15f
)