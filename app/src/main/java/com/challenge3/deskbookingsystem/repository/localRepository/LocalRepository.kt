package com.challenge3.deskbookingsystem.repository.localRepository

import com.challenge3.deskbookingsystem.repository.model.db.BookedDeskDbModel
import com.challenge3.deskbookingsystem.repository.model.db.BookedFixDesksDbModel

interface LocalRepository {
    suspend fun insertBookedDesk(desks: BookedDeskDbModel)
    fun getAllBookedDesks(): List<BookedDeskDbModel>
    suspend fun deleteBookedDeskById(id: String)
    suspend fun deleteAllDesks()

    //fix
    suspend fun insertFixdDesk(fixDesk: BookedFixDesksDbModel)
    fun getAllFixDesks(): List<BookedFixDesksDbModel>
    suspend fun deleteFixDeskById(id: String)
    suspend fun deleteAllFixDesks()

}

class LocalRepositoryImpl(private val dao: DeskBookingDao) : LocalRepository {
    override suspend fun insertBookedDesk(desks: BookedDeskDbModel) = dao.insertBookedDesk(desks)
    override fun getAllBookedDesks() = dao.getAllBookedDesks()
    override suspend fun deleteBookedDeskById(id: String) = dao.deleteById(id)
    override suspend fun deleteAllDesks() = dao.deleteAllBookedDesks()

    //fix
    override suspend fun insertFixdDesk(fixDesk: BookedFixDesksDbModel) = dao.insertFixDesk(fixDesk)

    override fun getAllFixDesks() = dao.getAllFixDesks()

    override suspend fun deleteFixDeskById(id: String) = dao.deleteFixById(id)

    override suspend fun deleteAllFixDesks() = dao.deleteAllFixDesks()

}