package com.challenge3.deskbookingsystem.repository.model.remote.desk

data class BookingOfDesk (
    val dateEnd: Long,
    val dateStart: Long,
        )
