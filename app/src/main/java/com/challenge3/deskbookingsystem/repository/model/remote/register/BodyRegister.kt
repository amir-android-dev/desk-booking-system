package com.challenge3.deskbookingsystem.repository.model.remote.register


import com.google.gson.annotations.SerializedName

data class BodyRegister(
    @SerializedName("department")
    val department: String?, // Diamir
    @SerializedName("email")
    val email: String?, // em@yahoo.com
    @SerializedName("firstname")
    val firstname: String?, // elit deserunt
    @SerializedName("lastname")
    val lastname: String?, // eu cillum labore occaecat
    @SerializedName("password")
    val password: String? // cupidatat dolor irure tempor aliquip
)