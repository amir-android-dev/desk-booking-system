package com.challenge3.deskbookingsystem.repository.model.remote.reservationsConfirm

data class BodyApprovedFixDesk(
    val id: String,
    val status: String
)