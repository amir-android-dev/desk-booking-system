package com.challenge3.deskbookingsystem.repository.model.remote.register

import com.google.gson.annotations.SerializedName
data class ResponseRegister(
    @SerializedName("createdAt")
    val createdAt: String?, // 2023-02-21T12:33:29.006Z
    @SerializedName("department")
    val department: String?, // Diamir
    @SerializedName("email")
    val email: String?, // em@yahoo.com
    @SerializedName("firstname")
    val firstname: String?, // elit deserunt
    @SerializedName("id")
    val id: String?, // 8c0347e2-a9ba-44e2-9780-3e318a00b08e
    @SerializedName("isAdmin")
    val isAdmin: Boolean?, // false
    @SerializedName("lastname")
    val lastname: String?, // eu cillum labore occaecat
    @SerializedName("updatedAt")
    val updatedAt: String? // 2023-02-21T12:33:29.006Z
)