package com.challenge3.deskbookingsystem.repository.model.remote.favorites

data class ResponseFavoritesItem(
    val createdAt: String,
    val desk: Desk,
    val id: String,
    val updatedAt: String,
    val user: User
)