package com.challenge3.deskbookingsystem.repository.remoteRepository

import com.challenge3.deskbookingsystem.repository.model.remote.login.BodyRefresh
import com.challenge3.deskbookingsystem.repository.model.remote.login.ResponseRefresh
import com.challenge3.deskbookingsystem.utils.Resource
import com.challenge3.deskbookingsystem.utils.toResource

interface RefreshTokenRepository {
    suspend fun refreshToken(body: BodyRefresh): Resource<ResponseRefresh>
}

class RefreshTokenRepositoryImpl(
    private val refreshTokenApiService: RefreshTokenApiService
): RefreshTokenRepository {

    override suspend fun refreshToken(body: BodyRefresh): Resource<ResponseRefresh> {
        return refreshTokenApiService.refreshToken(body).toResource()
    }
}