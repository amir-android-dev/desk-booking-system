package com.challenge3.deskbookingsystem.repository.model.remote.desk

import com.challenge3.deskbookingsystem.repository.model.remote.user.ResponseProfileOfCurrentUser

data class ResponseDeskBooking (
        val id: String,
        val bookedAt: String,
        val dateEnd: String,
        val dateStart: String,
        val desk: Desk,
        val user: ResponseProfileOfCurrentUser
        )