package com.challenge3.deskbookingsystem.repository.model.remote.desk

data class FixDeskRequest (
    val desk: String,
    val user: String,
    val id: String,
    val createdAt: String,
    val updatedAt: String,
    val status: String
        )