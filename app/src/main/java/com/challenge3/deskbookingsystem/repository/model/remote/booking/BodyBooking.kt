package com.challenge3.deskbookingsystem.repository.model.remote.booking

data class BodyBooking (
    val dateStart: String,
    val dateEnd: String,
    val desk: String,
        )