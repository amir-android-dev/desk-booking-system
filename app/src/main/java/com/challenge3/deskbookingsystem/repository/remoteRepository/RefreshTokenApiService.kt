package com.challenge3.deskbookingsystem.repository.remoteRepository

import com.challenge3.deskbookingsystem.repository.model.remote.login.BodyRefresh
import com.challenge3.deskbookingsystem.repository.model.remote.login.ResponseRefresh
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface RefreshTokenApiService {
    @POST("api/users/refresh")
    suspend fun refreshToken(@Body body: BodyRefresh): Response<ResponseRefresh>
}