package com.challenge3.deskbookingsystem.repository.model.remote.user


import com.google.gson.annotations.SerializedName

data class ResponseProfileOfCurrentUser(
    @SerializedName("createdAt")
    val createdAt: String?, // 2023-02-16T09:18:30.522Z
    @SerializedName("department")
    val department: String?, // DeepDive
    @SerializedName("email")
    val email: String?, // admin4@csaw.at
    @SerializedName("firstname")
    val firstname: String?, // Admin
    @SerializedName("id")
    val id: String?, // 84f533cf-0937-4da4-bc50-c8c5173bdc70
    @SerializedName("isAdmin")
    val isAdmin: Boolean?, // true
    @SerializedName("lastname")
    val lastname: String?, // User
    @SerializedName("updatedAt")
    val updatedAt: String? // 2023-02-23T11:03:05.879Z
)