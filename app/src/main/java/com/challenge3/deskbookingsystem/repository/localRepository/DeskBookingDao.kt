package com.challenge3.deskbookingsystem.repository.localRepository

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.challenge3.deskbookingsystem.repository.model.db.BookedDeskDbModel
import com.challenge3.deskbookingsystem.repository.model.db.BookedFixDesksDbModel
import com.challenge3.deskbookingsystem.utils.Constants

@Dao
interface DeskBookingDao {
    //flex
    @Insert
    suspend fun insertBookedDesk(desks: BookedDeskDbModel)

    @Query("select * from ${Constants.BOOKED_DESK_TABLE}")
    fun getAllBookedDesks(): List<BookedDeskDbModel>

    @Query("DELETE FROM ${Constants.BOOKED_DESK_TABLE} WHERE id = :id")
    suspend fun deleteById(id: String)

    @Query("DELETE FROM ${Constants.BOOKED_DESK_TABLE}")
    suspend fun deleteAllBookedDesks()

    //fix
    @Insert
    suspend fun insertFixDesk(fixDesks: BookedFixDesksDbModel)

    @Query("select * from ${Constants.BOOKED_FIX_DESK_TABLE}")
    fun getAllFixDesks(): List<BookedFixDesksDbModel>

    @Query("DELETE FROM ${Constants.BOOKED_FIX_DESK_TABLE} WHERE id = :id")
    suspend fun deleteFixById(id: String)

    @Query("DELETE FROM ${Constants.BOOKED_FIX_DESK_TABLE}")
    suspend fun deleteAllFixDesks()
}