package com.challenge3.deskbookingsystem.repository.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.challenge3.deskbookingsystem.repository.coreRepository.CoreRepository
import com.challenge3.deskbookingsystem.repository.model.remote.admin_comments.ResponseComments.ResponseCommentsItem

class CommentsPagingSource(private val coreRepository: CoreRepository) :
    PagingSource<Int, ResponseCommentsItem>() {

    override fun getRefreshKey(state: PagingState<Int, ResponseCommentsItem>): Int? {
        return null
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, ResponseCommentsItem> {
        return try {
            val currentPage = params.key ?: 0
            val response = coreRepository.getComments(currentPage)
            val comments = response.body() ?: emptyList()
            val responseComments = mutableListOf<ResponseCommentsItem>()
            responseComments.addAll(comments)

            if (response.isSuccessful) {
                LoadResult.Page(
                    data = responseComments,
                    prevKey = if (currentPage == 0) null else currentPage.minus(1),
                    nextKey = if (response.body()!!.isNotEmpty()) currentPage.plus(1) else null
                   // nextKey = currentPage.plus(1)
                )
            } else {
                LoadResult.Error(Throwable(response.message()))
            }
        } catch (e: Exception) {
            LoadResult.Error(Throwable(e.message))
        }
    }
}