package com.challenge3.deskbookingsystem.repository.model.remote.desk

import com.challenge3.deskbookingsystem.repository.model.remote.user.ResponseProfileOfCurrentUser

data class FixDesk (
    val id: String,
    val createdAt: String,
    val updatedAt: String,
    val status: String,
    val user: ResponseProfileOfCurrentUser,
    val updatedBy: UpdatedBy
        )
