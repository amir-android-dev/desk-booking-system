package com.challenge3.deskbookingsystem.repository.model.db

import androidx.room.TypeConverter

class StringListTypeConverter {
    @TypeConverter
    fun fromString(string: String?): List<String?>? {
        return string?.split(",")?.map { it.trim() }
    }

    @TypeConverter
    fun toString(list: List<String?>?): String? {
        return list?.joinToString(",")
    }
}