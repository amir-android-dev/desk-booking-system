package com.challenge3.deskbookingsystem.repository.model.remote.login

data class ResponseLogin(
    val token: String,
    val refresh: String,
    val statusCode: Int,
    val message: String
)
