package com.challenge3.deskbookingsystem.repository.remoteRepository

import com.challenge3.deskbookingsystem.repository.model.remote.admin_comments.ResponseComments
import com.challenge3.deskbookingsystem.repository.model.remote.bookedDesk.ResponseBookedDesksByUserList
import com.challenge3.deskbookingsystem.repository.model.remote.booking.BodyBooking
import com.challenge3.deskbookingsystem.repository.model.remote.bookedDesk.ResponseDeleteDeskById
import com.challenge3.deskbookingsystem.repository.model.remote.comment.BodyComment
import com.challenge3.deskbookingsystem.repository.model.remote.comment.ResponseComment
import com.challenge3.deskbookingsystem.repository.model.remote.desk.*
import com.challenge3.deskbookingsystem.repository.model.remote.desk.Desk
import com.challenge3.deskbookingsystem.repository.model.remote.favorites.BodyFavorites
import com.challenge3.deskbookingsystem.repository.model.remote.favorites.ResponseCreateFavorite
import com.challenge3.deskbookingsystem.repository.model.remote.login.BodyLogin
import com.challenge3.deskbookingsystem.repository.model.remote.login.ResponseLogin
import com.challenge3.deskbookingsystem.repository.model.remote.office.Office
import com.challenge3.deskbookingsystem.repository.model.remote.favorites.ResponseFavorites
import com.challenge3.deskbookingsystem.repository.model.remote.fixDesks.ResponseBookedFixDesks
import com.challenge3.deskbookingsystem.repository.model.remote.login.*
import com.challenge3.deskbookingsystem.repository.model.remote.office.BodyOffice
import com.challenge3.deskbookingsystem.repository.model.remote.register.BodyRegister
import com.challenge3.deskbookingsystem.repository.model.remote.register.ResponseRegister
import com.challenge3.deskbookingsystem.repository.model.remote.reservationsConfirm.*
import com.challenge3.deskbookingsystem.repository.model.remote.reservationsConfirm.ReservationsErrorBody
import com.challenge3.deskbookingsystem.repository.model.remote.user.ResponseProfileOfCurrentUser
import com.google.gson.JsonObject
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.*

interface ApiService {

    @POST("api/users/login")
    suspend fun login(@Body body: BodyLogin): Response<ResponseLogin>

    @POST("api/users/register")
    suspend fun registerUser(@Body body: BodyRegister): Response<ResponseRegister>

    //all booked desks of a user
    @GET("api/bookings/user/{id}")
    suspend fun getAllBookingsUserMade(@Path("id") userId: String): Response<ResponseBookedDesksByUserList>

    //booked fixDesks
    @GET("/api/fixdesk-requests/user/{userId}")
    suspend fun getAllBookingsFixDesks(@Path("userId") userId: String): Response<ResponseBookedFixDesks>

    @GET("api/users/profile")
    suspend fun getProfileOfCurrentUser(): Response<ResponseProfileOfCurrentUser>

    @GET("api/users/profile")
    suspend fun getProfileID(): Response<ResponseProfile>

    @POST("api/users/refresh")
    suspend fun refreshToken(@Body body: BodyRefresh): Response<ResponseRefresh>

    @GET("api/offices")
    suspend fun getAllOffices(): Response<List<Office>>

    //delete a booked desk
    @GET("api/users/profile")
    suspend fun loadProfileUser(): Response<ResponseProfileOfCurrentUser>

    @PUT("/api/users/{id}")
    suspend fun updateUserWithID(
        @Path("id") id: String,
        @Body body: BodyRegister
    ): Response<ResponseProfileOfCurrentUser>
    //cancel flex
    @DELETE("api/bookings/{id}")
    suspend fun deleteBookingWithId(@Path("id") id: String): Response<ResponseDeleteDeskById?>
    //cancel fix
    @DELETE("/api/fixdesk-requests/{id}")
    suspend fun deleteFixDeskById(@Path("id") id: String): Response<ResponseDeleteDeskById?>

    @GET("/api/favourites/user/{id}")
    suspend fun getAllFavorites(@Path("id") id: String): Response<ResponseFavorites>

    @DELETE("/api/favourites/{id}")
    suspend fun deleteFavoriteWithID(@Path("id") id: String): Response<String>

    //comment a desk
    @POST("api/comments")
    suspend fun postComment(@Body body: BodyComment): Response<ResponseComment>


    @GET("api/offices/{id}")
    suspend fun getOffice(@Path("id") officeId: String): Response<Office>

    @GET("api/desks")
    suspend fun getAllDesks(): Response<List<Desk>>

    @GET("api/bookings/desk/{id}")
    suspend fun getAllBookingsOfDesk(@Path("id") deskId: String): Response<List<ResponseDeskBooking>>

    @GET("api/desks/{id}")
    suspend fun getDesk(@Path("id") deskId: String): Response<Desk>

    @POST("api/bookings")
    suspend fun createBooking(@Body bodyBooking: BodyBooking): Response<ResponseDeskBooking>

    @GET("api/admin/fix-desk-requests")
    suspend fun getAllFixDeskRequests(): Response<GetAllFixDesk>

    @PUT("api/admin/fix-desk-requests")
    suspend fun approveFixDeskRequest(@Body body: BodyApprovedFixDesk): Response<ReservationsErrorBody>

    @DELETE("api/fixdesk-requests/{id}")
    suspend fun deleteFixDeskRequest(@Path("id") id: String): Response<String>

    //get all comments admin
    @GET("api/comments")
    suspend fun getComments(
        @Query("page") page: Int
    ): Response<ResponseComments>

    @POST("api/favourites")
    suspend fun createFavorite(@Body desk: BodyFavorites): Response<ResponseCreateFavorite>


    @POST("api/admin/offices")
    suspend fun createOffice(@Body bodyOffice: BodyOffice): Response<Office>

    @PUT("api/admin/offices/{id}")
    suspend fun putOffice(
        @Path("id") officeId: String,
        @Body bodyOffice: BodyOffice
    ): Response<Office>

    @POST("api/admin/desks")
    suspend fun createDesk(@Body bodyDesk: BodyDesk): Response<NewDeskResponse>

    @PUT("api/admin/desks/{id}")
    suspend fun putDesk(@Path("id") deskId: String, @Body bodyDesk: BodyDesk): Response<Desk>

    @GET("api/equipments")
    suspend fun getEquipment(): Response<JsonObject>

    @POST("api/fixdesk-requests")
    suspend fun createFixDeskRequest(@Body bodyFixDeskRequest: BodyFixDeskRequest): Response<FixDeskRequest>
}