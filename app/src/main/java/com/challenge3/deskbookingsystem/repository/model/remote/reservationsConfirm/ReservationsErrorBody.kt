package com.challenge3.deskbookingsystem.repository.model.remote.reservationsConfirm

data class ReservationsErrorBody(
    val id: String,
    val status: String
)