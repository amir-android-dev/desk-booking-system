package com.challenge3.deskbookingsystem.repository.model.remote.reservationsConfirm

data class GetAllFixDeskItem(
    val createdAt: String,
    val desk: Desk,
    val id: String,
    val status: String,
    val updatedAt: String,
    val updatedBy: Any,
    val user: User
)