package com.challenge3.deskbookingsystem.repository.model.remote.desk

data class UpdatedBy(
    val id: String,
    val firstname: String,
    val lastname: String,
    val email: String,
    val isAdmin: Boolean,
    val department: String,
    val createdAt: String,
    val updatedAt: String,
)
