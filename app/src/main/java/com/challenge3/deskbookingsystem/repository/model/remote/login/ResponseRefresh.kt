package com.challenge3.deskbookingsystem.repository.model.remote.login

data class ResponseRefresh(
   val token: String,
   val refresh: String
)
