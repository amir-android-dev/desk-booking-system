package com.challenge3.deskbookingsystem.repository.model.remote.login

data class BodyRefresh(
    val refresh: String
)
