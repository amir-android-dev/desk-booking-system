package com.challenge3.deskbookingsystem

import android.os.Bundle
import android.view.MenuItem
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import com.challenge3.deskbookingsystem.databinding.ActivityMainBinding
import com.challenge3.deskbookingsystem.ui.bottomNavigation.NavigationFragment

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setUpBarsAndNavigation()
    }

    private fun setUpBarsAndNavigation() {
        binding.mainToolbar.title = ""
        setSupportActionBar(binding.mainToolbar)
        setToolbarVisibility(false)
        setLogoVisibility(false)

        supportFragmentManager.beginTransaction()
            .replace(R.id.mainFragmentContainer, NavigationFragment())
            .commit()

        binding.ivProfile.setOnClickListener {
            findNavController(R.id.navHostFragmentContainer).navigate(R.id.action_global_profileFragment)
            setToolbarTitle(R.string.profileTitle)
            val frag =
                supportFragmentManager.findFragmentById(R.id.mainFragmentContainer) as NavigationFragment
            frag.uncheckAllItemsOfBottomNavigation()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                //todo maybe check if backstack > 0
                findNavController(R.id.navHostFragmentContainer).popBackStack()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    fun setToolbarTitle(titleId: Int) {
        setToolbarTitle(getString(titleId))
    }

    fun setToolbarTitle(title: String) {
        binding.toolbarTitle.text = title
    }

    fun setToolbarVisibility(isVisible: Boolean) {
        binding.mainToolbar.visibility = if (isVisible) VISIBLE else GONE
    }

    fun setLogoVisibility(isVisible: Boolean) {
        binding.diamirLogo.visibility = if (isVisible) VISIBLE else GONE
    }

    fun setProfileIconVisibility(isVisible: Boolean) {
        binding.ivProfile.visibility = if (isVisible) VISIBLE else GONE
    }

    fun setBackArrowVisibility(isVisible: Boolean) {
        supportActionBar?.setDisplayHomeAsUpEnabled(isVisible)
        supportActionBar?.setHomeButtonEnabled(isVisible)
    }

    fun setUpBarsForTopFragments(title: Int) {
        setToolbarTitle(getString(title))
        setToolbarVisibility(true)
        setLogoVisibility(true)
        setBackArrowVisibility(false)
        setProfileIconVisibility(true)
    }
}