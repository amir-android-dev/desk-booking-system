package com.challenge3.deskbookingsystem.ui.login

import android.content.Context
import android.content.SharedPreferences
import com.challenge3.deskbookingsystem.utils.Constants

class Token {

    fun saveToken(context: Context, token: String, refresh: String) {
        val sharedPreferences: SharedPreferences =
            context.getSharedPreferences(Constants.SHARE_PREFERENCES, Context.MODE_PRIVATE)
        sharedPreferences.edit()
            .putString("token", token)
            .putString("refresh", refresh)
            .apply()
    }

    fun loadToken(context: Context) {
        val sharedPreferences: SharedPreferences =
            context.getSharedPreferences(Constants.SHARE_PREFERENCES, Context.MODE_PRIVATE)
        val authTokenSharedPreferences = sharedPreferences.getString("token", null)
        sharedPreferences.getString("refresh", null)
        AuthTokenInterceptor.token = authTokenSharedPreferences
    }

    fun loadRefreshToken(context: Context): String {
        val authRefreshSharedPreferences: String
        val sharedPreferences: SharedPreferences =
            context.getSharedPreferences(Constants.SHARE_PREFERENCES, Context.MODE_PRIVATE)
        authRefreshSharedPreferences = sharedPreferences.getString("refresh", null).toString()
        AuthTokenInterceptor.refreshToken = authRefreshSharedPreferences
        return authRefreshSharedPreferences
    }

}