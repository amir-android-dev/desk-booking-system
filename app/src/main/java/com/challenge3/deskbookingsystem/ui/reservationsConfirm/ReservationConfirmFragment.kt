package com.challenge3.deskbookingsystem.ui.reservationsConfirm

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.challenge3.deskbookingsystem.MainActivity
import com.challenge3.deskbookingsystem.R
import com.challenge3.deskbookingsystem.databinding.FragmentReservationConfirmBinding
import com.challenge3.deskbookingsystem.repository.model.remote.reservationsConfirm.BodyApprovedFixDesk
import com.challenge3.deskbookingsystem.ui.bottomNavigation.NavigationFragment
import com.challenge3.deskbookingsystem.ui.favorites.RecyclerViewDeleteClickItem
import com.challenge3.deskbookingsystem.utils.displayToast
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class ReservationConfirmFragment : Fragment(), RecyclerViewDeleteClickItem {
    private val viewModel: ReservationConfirmViewModel by inject()
    private lateinit var binding: FragmentReservationConfirmBinding
    private lateinit var adapter: ReservationConfirmAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentReservationConfirmBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpBarsAndNavigation()

        viewModel.getAllFixDeskRequests()
        viewModel.list.observe(viewLifecycleOwner) {
            adapter = ReservationConfirmAdapter(this)
            binding.rvReservationConfirm.layoutManager = LinearLayoutManager(requireContext())
            binding.rvReservationConfirm.adapter = adapter
            adapter.submitList(it)
        }

        viewModel.error.observe(viewLifecycleOwner) { stringId ->
            displayToast(requireContext(), getString(stringId))
        }

        viewModel.responseMessage.observe(viewLifecycleOwner) { stringId ->
            displayToast(requireContext(), getString(stringId))
        }
    }

    override fun onClickDeleteItem(position: Int) {
        val dialog: AlertDialog = AlertDialog.Builder(requireView().context)
            .setTitle(getString(R.string.delete_request) + " ${adapter.currentList[position].desk.office.name} ${adapter.currentList[position].desk.label}")
            .setPositiveButton(getString(R.string.delete)) { dialog1, which ->
                lifecycleScope.launch {
                    val id = adapter.currentList[position].id
                    viewModel.deleteFixDeskRequest(id)
                }
            }
            .setNegativeButton("Cancel") { dialogInterface, i ->
                dialogInterface.cancel()
            }
            .create()
        dialog.show()
    }

    override fun onClickItem(position: Int) {
        val dialog: AlertDialog = AlertDialog.Builder(requireView().context)
            .setTitle(getString(R.string.approve_request) + " ${adapter.currentList[position].desk.office.name} ${adapter.currentList[position].desk.label}")
            .setPositiveButton(getString(R.string.approve)) { dialog1, which ->
                lifecycleScope.launch {
                    val id = adapter.currentList[position].id
                    viewModel.approveFixDesk(BodyApprovedFixDesk(id, "approved"))
                }
            }
            .setNegativeButton(getString(R.string.cancel)) { dialogInterface, i ->
                dialogInterface.cancel()
            }
            .create()
        dialog.show()
    }

    private fun setUpBarsAndNavigation() {
        val mainActivity = (requireActivity() as MainActivity)
        mainActivity.setToolbarTitle(R.string.booking_validation)
        mainActivity.setToolbarVisibility(true)
        mainActivity.setLogoVisibility(false)
        mainActivity.setProfileIconVisibility(true)
        mainActivity.setBackArrowVisibility(true)

        val navHostFragment = parentFragment as NavHostFragment?
        navHostFragment?.let { navHostFrag ->
            navHostFrag.parentFragment?.let { parentFrag ->
                val navigationFrag = parentFrag as NavigationFragment?
                navigationFrag?.setBottomNavigationBarVisibility(true)
            }
        }
    }
}