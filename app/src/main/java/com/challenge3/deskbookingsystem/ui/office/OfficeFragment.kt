package com.challenge3.deskbookingsystem.ui.office

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.challenge3.deskbookingsystem.MainActivity
import com.challenge3.deskbookingsystem.R
import com.challenge3.deskbookingsystem.databinding.FragmentOfficeBinding
import com.challenge3.deskbookingsystem.repository.model.remote.office.Office
import com.challenge3.deskbookingsystem.ui.bottomNavigation.NavigationFragment
import com.challenge3.deskbookingsystem.utils.ClickCallback
import com.challenge3.deskbookingsystem.utils.displayToast
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class OfficeFragment : Fragment() {
    private lateinit var binding: FragmentOfficeBinding
    private val adapter: OfficeAdapter by inject()
    private val viewModel: OfficeViewModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentOfficeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupBarsAndNavigation()

        adapter.setClickListener(object : ClickCallback {
            override fun onClick(id: String, title: String) {
                val action = OfficeFragmentDirections.actionOfficeFragmentToOfficeDesksFragment(
                    id, title
                )
                findNavController().navigate(action)
            }

        })
        binding.rvOffice.adapter = adapter
        binding.rvOffice.layoutManager = LinearLayoutManager(requireContext())

        viewModel.list.observe(viewLifecycleOwner) { list ->
            onOfficesUpdated(list)
        }

        viewModel.error.observe(viewLifecycleOwner) { stringId ->
            displayToast(requireContext(), getString(stringId))
        }
        viewModel.loadOffices()
    }

    private fun onOfficesUpdated(offices: List<Office>) {
        lifecycleScope.launch(Dispatchers.Main) {
            adapter.submitList(offices)
        }
    }

    private fun setupBarsAndNavigation() {
        (requireActivity() as MainActivity).setUpBarsForTopFragments(R.string.office)
        val navHostFragment = parentFragment as NavHostFragment?
        navHostFragment?.let { navHostFrag ->
            navHostFrag.parentFragment?.let { parentFrag ->
                val navigationFrag = parentFrag as NavigationFragment?
                navigationFrag?.setBottomNavigationBarVisibility(true)
            }
        }
    }
}