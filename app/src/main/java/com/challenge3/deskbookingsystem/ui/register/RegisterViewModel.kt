package com.challenge3.deskbookingsystem.ui.register

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.challenge3.deskbookingsystem.R
import com.challenge3.deskbookingsystem.repository.coreRepository.CoreRepository
import com.challenge3.deskbookingsystem.repository.model.remote.register.BodyRegister
import com.challenge3.deskbookingsystem.repository.model.remote.register.ResponseRegister
import com.challenge3.deskbookingsystem.repository.remoteRepository.RemoteRepositoryImpl.RemoteError
import com.challenge3.deskbookingsystem.utils.Resource
import com.challenge3.deskbookingsystem.utils.messageId
import kotlinx.coroutines.launch

class RegisterViewModel(private val coreRepo: CoreRepository) :
    ViewModel() {
    //by send a request(to control the progressbar)
    private val _isRegistered = MutableLiveData<Boolean>()
    val isRegistered: LiveData<Boolean>
        get() = _isRegistered

    private val _responseMsg = MutableLiveData<ResponseRegister>()
    val responseMsg: LiveData<ResponseRegister>
        get() = _responseMsg

    private val _error: MutableLiveData<Int> = MutableLiveData()
    val error: LiveData<Int>
        get() = _error

    fun registerUser(body: BodyRegister) {
        viewModelScope.launch {
            _isRegistered.postValue(true)
            when (val resource = coreRepo.registerUser(body)) {
                is Resource.Success -> {
                    _isRegistered.postValue(false)
                    _responseMsg.postValue(resource.result)
                }
                is Resource.Error -> {
                    _isRegistered.postValue(false)
                    when (resource.throwable) {
                        is RemoteError.ConflictError -> {
                            _error.postValue(R.string.user_with_this_email_exists)
                        }
                        is RemoteError.BadRequestError -> {
                            _error.postValue(R.string.check_email_is_correct)
                        }
                        else -> {
                            _error.postValue((resource.throwable as RemoteError).messageId())
                        }
                    }
                }
                Resource.SuccessNoBody -> {}
            }
        }
    }
}
