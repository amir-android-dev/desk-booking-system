package com.challenge3.deskbookingsystem.ui.officeDesks.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.challenge3.deskbookingsystem.repository.coreRepository.CoreRepository
import com.challenge3.deskbookingsystem.repository.model.remote.desk.BookingOfDesk
import com.challenge3.deskbookingsystem.repository.model.remote.desk.DeskWithBookings
import com.challenge3.deskbookingsystem.repository.model.remote.desk.ResponseDeskBooking
import com.challenge3.deskbookingsystem.repository.model.remote.office.Office
import com.challenge3.deskbookingsystem.repository.remoteRepository.RemoteRepositoryImpl
import com.challenge3.deskbookingsystem.utils.Resource
import com.challenge3.deskbookingsystem.utils.messageId
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class OfficeDesksViewModel(private val coreRepository: CoreRepository) : ViewModel() {
    private val _list: MutableLiveData<List<DeskWithBookings>> = MutableLiveData()
    val list: LiveData<List<DeskWithBookings>>
        get() = _list

    private val _error: MutableLiveData<Int> = MutableLiveData()
    val error: LiveData<Int>
        get() = _error

    fun loadAllDesksOfOffice(officeId: String, start: Long, end: Long) {
        viewModelScope.launch {
            when (val resource = coreRepository.getOffice(officeId)) {
                is Resource.Error -> {
                    _error.postValue((resource.throwable as RemoteRepositoryImpl.RemoteError).messageId())
                }
                is Resource.Success -> getAllBookingsOfDesk(resource, start, end)
                Resource.SuccessNoBody -> {}
            }
        }
    }

    // get bookings of every desk of room
    private suspend fun getAllBookingsOfDesk(
        resource: Resource.Success<Office>,
        start: Long,
        end: Long
    ) {
        val desksWithBookings = mutableListOf<DeskWithBookings>()
        if (resource.result.desks == null) return
        for (desk in resource.result.desks) {
            when (val bookingsOfDesk = coreRepository.getAllBookingsOfDesk(desk.id)) {
                is Resource.Error -> {}
                is Resource.Success -> {
                    val deskWithFixDesk =
                        getFixDeskOfDesk(desk.id, bookingsOfDesk.result)
                    if (deskWithFixDesk != null) {
                        desksWithBookings.add(deskWithFixDesk)
                    }
                }
                Resource.SuccessNoBody -> {}
            }
        }
        _list.value = desksWithBookings
        updateDeskAvailability(start, end)
    }

    // get FixDesk of every desk because the desk in the getOffice call does not return it with FixDesk
    // and transform it to DeskWithBookings
    private suspend fun getFixDeskOfDesk(
        id: String,
        bookingsOfDesk: List<ResponseDeskBooking>
    ): DeskWithBookings? {
        val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.GERMANY)
        return when (val deskWithFixDesk = coreRepository.getDesk(id)) {
            is Resource.Error -> {
                _error.postValue((deskWithFixDesk.throwable as RemoteRepositoryImpl.RemoteError).messageId())
                null
            }
            is Resource.Success -> {
                DeskWithBookings(
                    desk = deskWithFixDesk.result,
                    isAvailable = true, //show all by default
                    fixDesk = deskWithFixDesk.result.fixDesk,
                    bookings = bookingsOfDesk.map {
                        BookingOfDesk(
                            dateStart = format.parse(it.dateStart)!!.time,
                            dateEnd = format.parse(it.dateEnd)!!.time,
                        )
                    }
                )
            }
            Resource.SuccessNoBody -> {
                null
            }
        }
    }

    fun updateDeskAvailability(isFixEnabled: Boolean) {
        if (_list.value == null) return
        _list.value = _list.value?.map {
            DeskWithBookings(
                desk = it.desk,
                fixDesk = it.fixDesk,
                isAvailable = it.fixDesk == null || isFixEnabled,
                bookings = it.bookings
            )
        }
    }

    fun updateDeskAvailability(start: Long, end: Long) {
        if (_list.value == null) return
        _list.value = _list.value?.map {
            DeskWithBookings(
                desk = it.desk,
                fixDesk = it.fixDesk,
                //desk is available if no bookings overlap the chosen time span
                isAvailable = (it.bookings.isEmpty() || it.bookings.filter { booking ->
                    booking.dateEnd < start || booking.dateStart > end
                }.size == it.bookings.size),
                bookings = it.bookings
            )
        }
    }
}