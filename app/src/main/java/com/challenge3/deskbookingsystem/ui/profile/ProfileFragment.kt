package com.challenge3.deskbookingsystem.ui.profile

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.challenge3.deskbookingsystem.MainActivity
import com.challenge3.deskbookingsystem.R
import com.challenge3.deskbookingsystem.databinding.FragmentProfileBinding
import com.challenge3.deskbookingsystem.repository.model.remote.register.BodyRegister
import com.challenge3.deskbookingsystem.ui.bottomNavigation.NavigationFragment
import com.challenge3.deskbookingsystem.ui.login.LoginViewModel
import com.challenge3.deskbookingsystem.utils.displayToast
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.activityViewModel

class ProfileFragment : Fragment() {
    private val viewModel: ProfileViewModel by inject()
    private val loginViewModel: LoginViewModel by activityViewModel()
    private lateinit var binding: FragmentProfileBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupBarsAndNavigation()

        spinnerSetup()
        observerUpdateProfile()
        viewModel.loadUserProfile()

        binding.tvLogout.setOnClickListener {
            viewModel.deleteToken(requireContext())
            //to stop navigation we need to set isLogged to false
            loginViewModel.setIsLogged(false)
            val action = ProfileFragmentDirections.actionGlobalLoginFragment()
            findNavController().navigate(action)
        }


        binding.buSave.setOnClickListener {
            if (binding.etPassword.text.isNullOrBlank() || binding.etPasswordConfirm.text.isNullOrBlank()) {
                displayToast(requireContext(), getString(R.string.password_must_not_be_empty))
                binding.etPassword.setHintTextColor(Color.RED)
                binding.etPasswordConfirm.setHintTextColor(Color.RED)

            } else
                if (binding.etPassword.text.toString() == binding.etPasswordConfirm.text.toString()) {
                    viewModel.updateUserWithID(
                        BodyRegister(
                            binding.spinnerDepartment.selectedItem.toString(),
                            binding.etEmail.text.toString(),
                            binding.etFirstName.text.toString(),
                            binding.etLastName.text.toString(),
                            binding.etPassword.text.toString()
                        )
                    )
                } else {
                    displayToast(requireContext(), getString(R.string.password_do_not_match))
                }
        }

        binding.tvEditProfile.setOnClickListener {
            binding.parentView.visibility = View.VISIBLE
            binding.childView.visibility = View.VISIBLE
        }

        binding.buCancel.setOnClickListener {
            binding.parentView.visibility = View.GONE
            binding.childView.visibility = View.GONE

        }
    }

    //setup spinner
    private fun spinnerSetup() {
        val spinner: Spinner = binding.spinnerDepartment
        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.departments,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
        }
    }

    private fun observerUpdateProfile() {
        viewModel.responseMessage.observe(viewLifecycleOwner) { message ->
            displayToast(requireContext(), getString(message))
        }

        viewModel.profile.observe(viewLifecycleOwner) { user ->
            lifecycleScope.launch {

                binding.tvFirstName.text = user.firstname
                binding.tvLastName.text = user.lastname
                binding.tvEmail.text = user.email
                binding.tvDepartment.text = user.department

                binding.etFirstName.setText(user.firstname)
                binding.etLastName.setText(user.lastname)
                binding.etEmail.setText(user.email)
                binding.spinnerDepartment.setSelection(
                    (binding.spinnerDepartment.adapter as ArrayAdapter<String>).getPosition(
                        user.department
                    )
                )
                binding.parentView.visibility = View.GONE
                binding.childView.visibility = View.GONE
            }
        }

        viewModel.error.observe(viewLifecycleOwner) { stringId ->
            lifecycleScope.launch(Dispatchers.Main) {
                displayToast(requireContext(), getString(stringId))
            }
        }
    }

    private fun setupBarsAndNavigation() {
        (requireActivity() as MainActivity).setUpBarsForTopFragments(R.string.profileTitle)
        val navHostFragment = parentFragment as NavHostFragment?
        navHostFragment?.let { navHostFrag ->
            navHostFrag.parentFragment?.let { parentFrag ->
                val navigationFrag = parentFrag as NavigationFragment?
                navigationFrag?.setBottomNavigationBarVisibility(true)
            }
        }
    }
}


