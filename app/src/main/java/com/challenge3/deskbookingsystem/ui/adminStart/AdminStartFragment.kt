package com.challenge3.deskbookingsystem.ui.adminStart

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.challenge3.deskbookingsystem.MainActivity
import com.challenge3.deskbookingsystem.R
import com.challenge3.deskbookingsystem.databinding.FragmentAdminStartBinding

class AdminStartFragment : Fragment() {
    private lateinit var binding: FragmentAdminStartBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAdminStartBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (requireActivity() as MainActivity).setUpBarsForTopFragments(R.string.admin)

        //navigate to comments
        binding.btnComments.setOnClickListener {
            val action =
                AdminStartFragmentDirections.actionAdminStartFragmentToAdminCommentsFragment()
            findNavController().navigate(action)
        }

        binding.btnValidation.setOnClickListener {
            val action =
                AdminStartFragmentDirections.actionAdminStartFragmentToReservationConfirmFragment2()
            findNavController().navigate(action)
        }

        binding.btnEdit.setOnClickListener {
            val action =
                AdminStartFragmentDirections.actionAdminStartFragmentToAdminOfficesFragment()
            findNavController().navigate(action)
        }
    }
}