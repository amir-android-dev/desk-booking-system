package com.challenge3.deskbookingsystem.ui.officeDesks.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.challenge3.deskbookingsystem.R
import com.challenge3.deskbookingsystem.repository.coreRepository.CoreRepository
import com.challenge3.deskbookingsystem.repository.model.remote.booking.BodyBooking
import com.challenge3.deskbookingsystem.repository.model.remote.desk.BodyFixDeskRequest
import com.challenge3.deskbookingsystem.repository.model.remote.desk.Desk
import com.challenge3.deskbookingsystem.repository.model.remote.favorites.BodyFavorites
import com.challenge3.deskbookingsystem.repository.model.remote.user.ResponseProfileOfCurrentUser
import com.challenge3.deskbookingsystem.repository.remoteRepository.RemoteRepositoryImpl.*
import com.challenge3.deskbookingsystem.utils.Resource
import com.challenge3.deskbookingsystem.utils.messageId
import kotlinx.coroutines.launch

class DeskDetailViewModel(private val coreRepository: CoreRepository) : ViewModel() {
    private val _desk: MutableLiveData<Desk> = MutableLiveData()
    val desk: LiveData<Desk>
        get() = _desk

    private val _error: MutableLiveData<Int> = MutableLiveData()
    val error: LiveData<Int>
        get() = _error

    private val _bookedSuccess: MutableLiveData<Boolean> = MutableLiveData()
    val bookedSuccess: LiveData<Boolean>
        get() = _bookedSuccess

    private val _profile: MutableLiveData<ResponseProfileOfCurrentUser> = MutableLiveData()
    val profile: LiveData<ResponseProfileOfCurrentUser>
        get() = _profile

    private val _successMessage = MutableLiveData<Int>()
    val successMessage: LiveData<Int>
        get() = _successMessage

    fun loadDesk(deskId: String) {
        viewModelScope.launch {
            when (val resource = coreRepository.getDesk(deskId)) {
                is Resource.Error -> {
                    _error.postValue((resource.throwable as RemoteError).messageId())
                }
                is Resource.Success -> _desk.postValue(resource.result)
                Resource.SuccessNoBody -> {}
            }
        }
    }

    fun bookDesk(deskId: String, dateStart: String, dateEnd: String) {
        viewModelScope.launch {
            val bodyBooking = BodyBooking(deskId, dateStart, dateEnd)
            when (
                val resource = coreRepository.createBooking(bodyBooking)) {
                is Resource.Error -> {
                    if (resource.throwable is RemoteError.ConflictError) {
                        _error.postValue(R.string.error_conflict_desk)
                    } else {
                        _error.postValue((resource.throwable as RemoteError).messageId())
                    }
                }
                is Resource.Success -> {
                    _bookedSuccess.postValue(true)
                }
                Resource.SuccessNoBody -> {}
            }
        }
    }

    fun loadUser() {
        viewModelScope.launch {
            when (val resource = coreRepository.getProfileOfCurrentUser()) {
                is Resource.Error -> {
                    _error.postValue((resource.throwable as RemoteError).messageId())
                }
                is Resource.Success -> {
                    _profile.postValue(resource.result)
                }
                Resource.SuccessNoBody -> {}
            }
        }
    }

    fun createFavorite(desk: BodyFavorites) {
        viewModelScope.launch {
            when (val resource = coreRepository.createFavorite(desk)) {
                is Resource.Error -> {
                    _error.postValue((resource.throwable as RemoteError).messageId())
                }
                is Resource.Success -> {
                    _successMessage.postValue(R.string.aded_favorites)
                }
                Resource.SuccessNoBody -> {}
            }
        }
    }

    fun createFixDesk(deskId: String) {
        viewModelScope.launch {
            val bodyFixDeskRequest = BodyFixDeskRequest(deskId)
            when (val resource = coreRepository.createFixDesk(bodyFixDeskRequest)) {
                is Resource.Error -> {
                    if (resource.throwable is RemoteError.ConflictError) {
                        _error.postValue(R.string.error_fixdesk)
                    } else {
                        _error.postValue((resource.throwable as RemoteError).messageId())
                    }
                }
                is Resource.Success -> {
                    _bookedSuccess.postValue(true)
                }
                Resource.SuccessNoBody -> {}
            }
        }
    }
}