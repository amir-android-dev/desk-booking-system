package com.challenge3.deskbookingsystem.ui.adminEdit.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.challenge3.deskbookingsystem.R
import com.challenge3.deskbookingsystem.repository.coreRepository.CoreRepository
import com.challenge3.deskbookingsystem.repository.model.remote.desk.Desk
import com.challenge3.deskbookingsystem.repository.model.remote.desk.NewDeskResponse
import com.challenge3.deskbookingsystem.repository.model.remote.office.BodyOffice
import com.challenge3.deskbookingsystem.repository.model.remote.office.Office
import com.challenge3.deskbookingsystem.repository.remoteRepository.RemoteRepositoryImpl
import com.challenge3.deskbookingsystem.repository.remoteRepository.RemoteRepositoryImpl.*
import com.challenge3.deskbookingsystem.utils.Resource
import com.challenge3.deskbookingsystem.utils.messageId
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch

class AdminEditOfficeViewModel(private val coreRepository: CoreRepository) : ViewModel() {
    private val _office: MutableSharedFlow<Office> =
        MutableSharedFlow(0, 1, BufferOverflow.DROP_LATEST)
    val office: SharedFlow<Office>
        get() = _office

    private val _desk: MutableSharedFlow<NewDeskResponse> =
        MutableSharedFlow(0, 1, BufferOverflow.DROP_LATEST)
    val desk: SharedFlow<NewDeskResponse>
        get() = _desk

    private val _error: MutableLiveData<Int> = MutableLiveData()
    val error: LiveData<Int>
        get() = _error

    fun loadOffice(officeId: String) {
        viewModelScope.launch {
            when (val resource = coreRepository.getOffice(officeId)) {
                is Resource.Error -> {
                    _error.postValue((resource.throwable as RemoteError).messageId())
                }
                is Resource.Success -> _office.emit(resource.result)
                Resource.SuccessNoBody -> {}
            }
        }
    }

    fun createDesk(deskId: String, officeId: String) {
        viewModelScope.launch {
            when (val resource = coreRepository.createDesk(deskId, officeId)) {
                is Resource.Error -> {
                    if (resource.throwable is RemoteError.BadRequestError) {
                        _error.postValue(R.string.error_edit_office)
                    } else {
                        _error.postValue((resource.throwable as RemoteError).messageId())
                    }
                }
                is Resource.Success -> _desk.emit(resource.result)
                Resource.SuccessNoBody -> {}
            }
        }
    }

    fun editOffice(officeId: String, bodyOffice: BodyOffice) {
        viewModelScope.launch {
            when (val resource = coreRepository.editOffice(officeId, bodyOffice)) {
                is Resource.Error -> {
                    _error.postValue((resource.throwable as RemoteError).messageId())
                }
                is Resource.Success -> _office.emit(resource.result)
                Resource.SuccessNoBody -> {}
            }
        }
    }
}