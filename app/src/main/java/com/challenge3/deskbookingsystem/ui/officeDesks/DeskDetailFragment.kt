package com.challenge3.deskbookingsystem.ui.officeDesks

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.challenge3.deskbookingsystem.MainActivity
import com.challenge3.deskbookingsystem.R
import com.challenge3.deskbookingsystem.databinding.DialogBookDeskBinding
import com.challenge3.deskbookingsystem.databinding.FragmentDeskDetailBinding
import com.challenge3.deskbookingsystem.repository.model.remote.favorites.BodyFavorites
import com.challenge3.deskbookingsystem.ui.bottomNavigation.NavigationFragment
import com.challenge3.deskbookingsystem.ui.officeDesks.adapter.EquipmentAdapter
import com.challenge3.deskbookingsystem.ui.officeDesks.viewmodel.DeskDetailViewModel
import com.challenge3.deskbookingsystem.utils.displayToast
import com.challenge3.deskbookingsystem.utils.formatDateToString
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import java.text.SimpleDateFormat
import java.util.*

class DeskDetailFragment : Fragment() {
    private lateinit var binding: FragmentDeskDetailBinding
    private val adapter: EquipmentAdapter by inject()
    private val viewModel: DeskDetailViewModel by inject()
    private val args: DeskDetailFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDeskDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpBarsAndNavigation()

        binding.rvEquipment.adapter = adapter
        binding.rvEquipment.layoutManager = LinearLayoutManager(requireContext())
        val timeRange =
            "${formatDateToString(args.startDate)} - ${formatDateToString(args.endDate)}"
        binding.tvBookingTimeInfo.text = timeRange

        if (args.isFix) {
            binding.tvTypeInfo.text = getString(R.string.fix)
        } else {
            binding.tvTypeInfo.text = getString(R.string.flex)
        }

        binding.btnBookDesk.setOnClickListener {
            setUpDialog()
        }

        binding.chFavorite.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                viewModel.createFavorite(BodyFavorites(args.deskId))
            } else {
                //remove from Favorites?
            }
        }

        viewModel.successMessage.observe(viewLifecycleOwner) { stringId ->
            displayToast(requireContext(), getString(stringId))
        }

        viewModel.desk.observe(viewLifecycleOwner) { desk ->
            lifecycleScope.launch(Dispatchers.Main) {
                adapter.submitList(desk.equipment)
                binding.tvLabelInfo.text = desk.label
                binding.tvOfficeInfo.text = desk.office.name
            }
        }

        viewModel.bookedSuccess.observe(viewLifecycleOwner) { navigate ->
            navigateToUserBookings()
        }

        viewModel.error.observe(viewLifecycleOwner) { stringId ->
            displayToast(requireContext(), getString(stringId))
        }
        viewModel.loadDesk(args.deskId)
        viewModel.loadUser()
    }

    private fun setUpDialog() {
        val builder = MaterialAlertDialogBuilder(requireContext())
        val dialogBookDeskBinding =
            DialogBookDeskBinding.inflate(LayoutInflater.from(requireContext()))
        builder.setView(dialogBookDeskBinding.root)
        val dialog = builder.create()

        if (args.isFix) {
            dialogBookDeskBinding.btnBook.text = getString(R.string.book_as_fix)
        }

        val timeRangeDialog =
            "${formatDateToString(args.startDate)} - ${formatDateToString(args.endDate)}"
        dialogBookDeskBinding.tvDialog.text = timeRangeDialog
        dialogBookDeskBinding.etUserName.setText(
            getString(
                R.string.full_name,
                viewModel.profile.value?.firstname,
                viewModel.profile.value?.lastname
            )
        )
        dialogBookDeskBinding.etEmail.setText(viewModel.profile.value?.email)

        dialogBookDeskBinding.btnBook.setOnClickListener {
            val formatApi = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.GERMANY)
            val startDate = formatApi.format(args.startDate.time)
            val endDate = formatApi.format(args.endDate.time)
            if (args.isFix) {
                viewModel.createFixDesk(args.deskId)
            } else {
                viewModel.bookDesk(startDate, endDate, args.deskId)
            }
            dialog.dismiss()
        }
        dialogBookDeskBinding.btnDialogCancel.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun navigateToUserBookings() {
        lifecycleScope.launch(Dispatchers.Main) {
            val action = DeskDetailFragmentDirections.actionGlobalUserBookingsFragment()
            findNavController().navigate(action)
        }
    }

    private fun setUpBarsAndNavigation() {
        val mainActivity = (requireActivity() as MainActivity)
        mainActivity.setToolbarTitle(args.deskName)
        mainActivity.setToolbarVisibility(true)
        mainActivity.setLogoVisibility(false)
        mainActivity.setProfileIconVisibility(true)
        mainActivity.setBackArrowVisibility(true)

        val navHostFragment = parentFragment as NavHostFragment?
        navHostFragment?.let { navHostFrag ->
            navHostFrag.parentFragment?.let { parentFrag ->
                val navigationFrag = parentFrag as NavigationFragment?
                navigationFrag?.setBottomNavigationBarVisibility(true)
            }
        }
    }
}