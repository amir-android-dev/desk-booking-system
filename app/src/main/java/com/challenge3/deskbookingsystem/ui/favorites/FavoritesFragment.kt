package com.challenge3.deskbookingsystem.ui.favorites

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.challenge3.deskbookingsystem.MainActivity
import com.challenge3.deskbookingsystem.R
import com.challenge3.deskbookingsystem.databinding.FragmentFavoritesBinding
import com.challenge3.deskbookingsystem.ui.bottomNavigation.NavigationFragment
import com.challenge3.deskbookingsystem.ui.login.AuthTokenInterceptor
import com.challenge3.deskbookingsystem.utils.displayToast
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class FavoritesFragment : Fragment(), RecyclerViewDeleteClickItem {

    private val viewModel: FavoritesViewModel by inject()
    private lateinit var binding: FragmentFavoritesBinding
    private var adapter = FavoriteAdapter(this)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentFavoritesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupBarsAndNavigation()

        viewModel.list.observe(viewLifecycleOwner) {
            val rvFavorites = binding.rvFavorites
            binding.rvFavorites.layoutManager = LinearLayoutManager(requireContext())
            rvFavorites.adapter = adapter
            adapter.submitList(it)
        }
        viewModel.getAllFavorites(AuthTokenInterceptor.profileID.toString())

        viewModel.deletedMsg.observe(viewLifecycleOwner) { stringId ->
            displayToast(requireContext(), getString(stringId))
        }

        viewModel.error.observe(viewLifecycleOwner) { stringId ->
            displayToast(requireContext(), getString(stringId))
        }
    }

    private fun setupBarsAndNavigation() {
        (requireActivity() as MainActivity).setUpBarsForTopFragments(R.string.favoritesTitle)
        val navHostFragment = parentFragment as NavHostFragment?
        navHostFragment?.let { navHostFrag ->
            navHostFrag.parentFragment?.let { parentFrag ->
                val navigationFrag = parentFrag as NavigationFragment?
                navigationFrag?.setBottomNavigationBarVisibility(true)
            }
        }
    }

    override fun onClickDeleteItem(position: Int) {
        lifecycleScope.launch {
            val favoriteToDelete = adapter.currentList[position].id
            val dialog: AlertDialog = AlertDialog.Builder(requireView().context)
                .setTitle(getString(R.string.delete_favorite) + "${adapter.currentList[position].desk.office.name}")
                .setPositiveButton(getString(R.string.delete)) { dialog1, which ->
                    lifecycleScope.launch {
                        viewModel.deleteFavoriteByID(favoriteToDelete)
                    }
                }
                .setNegativeButton(getString(R.string.cansel)) { dialogInterface, i ->
                    dialogInterface.cancel()
                }
                .create()
            dialog.show()
        }
    }

    override fun onClickItem(position: Int) {
        val id = adapter.currentList[position].desk.office.id
        val title = adapter.currentList[position].desk.office.name
        val action = FavoritesFragmentDirections.actionFavoritesFragmentToOfficeDesksFragment(
            id, title
        )
        findNavController().navigate(action)
    }
}