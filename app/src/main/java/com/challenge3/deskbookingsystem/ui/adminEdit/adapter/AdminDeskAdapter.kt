package com.challenge3.deskbookingsystem.ui.adminEdit.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.challenge3.deskbookingsystem.databinding.ItemAdminDesksBinding
import com.challenge3.deskbookingsystem.repository.model.remote.desk.Desk
import com.challenge3.deskbookingsystem.utils.ClickCallback

class AdminDeskAdapter() :
    ListAdapter<Desk, AdminDeskAdapter.AdminDesksViewHolder>(AdminDeskWithOfficeDiffUtil()) {
    private var clickCallback: ClickCallback? = null

    class AdminDesksViewHolder(val binding: ItemAdminDesksBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(desk: Desk, clickCallback: ClickCallback?) {
            binding.tvDeskName.text = desk.label
            binding.tvDeskInfo.text =
                desk.equipment.toString().replace("[", "").replace("]", "")
            binding.cvEditDesk.setOnClickListener {
                clickCallback?.onClick(desk.id, desk.label)
            }
        }
    }

    fun setClickListener(clickCallback: ClickCallback) {
        this.clickCallback = clickCallback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdminDesksViewHolder {
        val binding =
            ItemAdminDesksBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AdminDesksViewHolder(binding)
    }

    override fun onBindViewHolder(holder: AdminDesksViewHolder, position: Int) {
        holder.bind(currentList[position], clickCallback)
    }

    override fun getItemCount(): Int {
        return currentList.size
    }
}

class AdminDeskWithOfficeDiffUtil() : DiffUtil.ItemCallback<Desk>() {
    override fun areItemsTheSame(oldItem: Desk, newItem: Desk): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Desk, newItem: Desk): Boolean {
        return oldItem == newItem
    }
}