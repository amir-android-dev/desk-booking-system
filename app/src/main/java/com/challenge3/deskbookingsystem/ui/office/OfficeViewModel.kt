package com.challenge3.deskbookingsystem.ui.office

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.challenge3.deskbookingsystem.repository.coreRepository.CoreRepository
import com.challenge3.deskbookingsystem.repository.model.remote.office.Office
import com.challenge3.deskbookingsystem.repository.remoteRepository.RemoteRepositoryImpl
import com.challenge3.deskbookingsystem.utils.Resource
import com.challenge3.deskbookingsystem.utils.messageId
import kotlinx.coroutines.launch

class OfficeViewModel(private val coreRepository: CoreRepository) : ViewModel() {
    private val _list: MutableLiveData<List<Office>> = MutableLiveData()
    val list: LiveData<List<Office>>
        get() = _list

    private val _error: MutableLiveData<Int> = MutableLiveData()
    val error: LiveData<Int>
        get() = _error

    fun loadOffices() {
        viewModelScope.launch {
            when (val resource = coreRepository.getAllOffices()) {
                is Resource.Error -> {
                    _error.postValue((resource.throwable as RemoteRepositoryImpl.RemoteError).messageId())
                }
                is Resource.Success -> _list.postValue(resource.result)
                Resource.SuccessNoBody -> {}
            }
        }
    }
}