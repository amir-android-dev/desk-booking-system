package com.challenge3.deskbookingsystem.ui.login

import android.content.Context
import android.text.TextUtils
import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.challenge3.deskbookingsystem.repository.coreRepository.CoreRepository
import com.challenge3.deskbookingsystem.repository.model.remote.login.BodyLogin
import com.challenge3.deskbookingsystem.repository.model.remote.login.ResponseProfile
import com.challenge3.deskbookingsystem.repository.remoteRepository.RemoteRepositoryImpl.RemoteError
import com.challenge3.deskbookingsystem.utils.Resource
import com.challenge3.deskbookingsystem.utils.messageId
import kotlinx.coroutines.launch

class LoginViewModel(private val coreRepo: CoreRepository) :
    ViewModel() {
    private val _isLogged = MutableLiveData<Boolean>()
    val isLogged: LiveData<Boolean>
        get() = _isLogged

    private val _user = MutableLiveData<ResponseProfile>()
    val user: LiveData<ResponseProfile>
        get() = _user

    private val _error: MutableLiveData<Int> = MutableLiveData()
    val error: LiveData<Int>
        get() = _error

    fun login(bodyLogin: BodyLogin, context: Context) {
        _isLogged.postValue(false)
        viewModelScope.launch {
            when (val resource = coreRepo.login(bodyLogin)) {
                is Resource.Error -> {
                    _error.postValue((resource.throwable as RemoteError).messageId())
                }
                is Resource.Success -> {
                    Token().saveToken(
                        context,
                        resource.result.token,
                        resource.result.refresh
                    )

                    Token().loadToken(context.applicationContext)
                    getProfileId()
                }
                Resource.SuccessNoBody -> {}
            }
        }
    }

    fun isValidEmail(target: CharSequence?): Boolean {
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target.toString())
            .matches()
    }

    fun getProfileId() {
        viewModelScope.launch {
            when (val resource = coreRepo.getProfileID()) {
                is Resource.Error -> {
                    if (resource.throwable is RemoteError.NoConnectionError) {
                        _error.postValue((resource.throwable as RemoteError).messageId())
                        _isLogged.postValue(true)
                    }
                }
                is Resource.Success -> {
                    val id = resource.result.id
                    AuthTokenInterceptor.profileID = id
                    val result = resource.result
                    _user.postValue(result)
                    _isLogged.postValue(true)
                }
                Resource.SuccessNoBody -> {}
            }
        }
    }

    fun setIsLogged(isLogged: Boolean) {
        _isLogged.postValue(isLogged)
    }
}



