package com.challenge3.deskbookingsystem.ui.officeDesks.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.challenge3.deskbookingsystem.R
import com.challenge3.deskbookingsystem.databinding.ItemOfficeDeskBinding
import com.challenge3.deskbookingsystem.repository.model.remote.desk.DeskWithBookings
import com.challenge3.deskbookingsystem.utils.ClickCallback

class OfficeDesksAdapter(private val context: Context) :
    ListAdapter<DeskWithBookings, OfficeDesksAdapter.OfficeDesksViewHolder>(DeskWithOfficeDiffUtil()) {
    private var clickCallback: ClickCallback? = null

    class OfficeDesksViewHolder(val binding: ItemOfficeDeskBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            deskWithBookings: DeskWithBookings,
            context: Context,
            clickCallback: ClickCallback?
        ) {
            binding.rvDesksName.text = deskWithBookings.desk.label

            //not available desk and fix desks should not be clickable
            if (!deskWithBookings.isAvailable) {
                binding.rvInfo.text = context.getString(R.string.flex)
                binding.cvOfficeDesks.foreground =
                    ContextCompat.getDrawable(context, R.color.grey_transparent)

            } else if (deskWithBookings.desk.fixDesk != null) {
                binding.rvInfo.text = context.getString(R.string.fix)
                binding.cvOfficeDesks.foreground =
                    ContextCompat.getDrawable(context, R.color.grey_transparent)

            } else {
                binding.rvInfo.text = context.getString(R.string.flex)
                binding.cvOfficeDesks.foreground =
                    ContextCompat.getDrawable(context, R.color.transparent)
                binding.cvOfficeDesks.setOnClickListener {
                    clickCallback?.onClick(deskWithBookings.desk.id, deskWithBookings.desk.label)
                }
            }
        }
    }

    fun setClickListener(clickCallback: ClickCallback) {
        this.clickCallback = clickCallback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OfficeDesksViewHolder {
        val binding =
            ItemOfficeDeskBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return OfficeDesksViewHolder(binding)
    }

    override fun onBindViewHolder(holder: OfficeDesksViewHolder, position: Int) {
        holder.bind(currentList[position], context, clickCallback)
    }

    override fun getItemCount(): Int {
        return currentList.size
    }
}

class DeskWithOfficeDiffUtil() : DiffUtil.ItemCallback<DeskWithBookings>() {
    override fun areItemsTheSame(oldItem: DeskWithBookings, newItem: DeskWithBookings): Boolean {
        return oldItem.desk.id == newItem.desk.id
    }

    override fun areContentsTheSame(oldItem: DeskWithBookings, newItem: DeskWithBookings): Boolean {
        return oldItem == newItem
    }
}