package com.challenge3.deskbookingsystem.ui.adminEdit

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.coroutineScope
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.challenge3.deskbookingsystem.MainActivity
import com.challenge3.deskbookingsystem.R
import com.challenge3.deskbookingsystem.databinding.DialogEditOfficeBinding
import com.challenge3.deskbookingsystem.databinding.FragmentAdminOfficesBinding
import com.challenge3.deskbookingsystem.repository.model.remote.office.Office
import com.challenge3.deskbookingsystem.ui.adminEdit.viewModel.AdminOfficeViewModel
import com.challenge3.deskbookingsystem.ui.bottomNavigation.NavigationFragment
import com.challenge3.deskbookingsystem.ui.office.OfficeAdapter
import com.challenge3.deskbookingsystem.utils.ClickCallback
import com.challenge3.deskbookingsystem.utils.displayToast
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class AdminOfficesFragment : Fragment() {
    private val viewModel: AdminOfficeViewModel by inject()
    private val adapter: OfficeAdapter by inject()
    private lateinit var binding: FragmentAdminOfficesBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAdminOfficesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpBarsAndNavigation()

        binding.btnNewOffice.setOnClickListener {
            showDialog()
        }

        adapter.setClickListener(object : ClickCallback {
            override fun onClick(id: String, title: String) {
                navigate(id, title)
            }
        })

        binding.rvEditOffice.adapter = adapter
        binding.rvEditOffice.layoutManager = LinearLayoutManager(requireContext())

        viewModel.list.onEach { list ->
            onOfficesUpdated(list)
        }.launchIn(viewLifecycleOwner.lifecycle.coroutineScope)

        viewModel.office.onEach { office ->
            Log.d("Navigation", office.name)
            navigate(office.id, office.name)
        }.launchIn(viewLifecycleOwner.lifecycle.coroutineScope)


        viewModel.error.observe(viewLifecycleOwner) { stringId ->
            displayToast(requireContext(), getString(stringId))
        }
        viewModel.loadOffices()
    }

    private fun showDialog() {
        val builder = MaterialAlertDialogBuilder(requireContext())
        val dialogEditOfficeBinding =
            DialogEditOfficeBinding.inflate(LayoutInflater.from(requireContext()))
        builder.setView(dialogEditOfficeBinding.root)
        val dialog = builder.create()

        dialogEditOfficeBinding.tvDialogHeader.text =
            getString(R.string.edit_office_name)
        dialogEditOfficeBinding.etDialog.hint = getString(R.string.office_name)

        dialogEditOfficeBinding.btnConfirm.setOnClickListener {
            if (dialogEditOfficeBinding.etDialog.text != null
                && dialogEditOfficeBinding.etDialog.text.toString().isNotEmpty()
            ) {
                viewModel.createOffice(dialogEditOfficeBinding.etDialog.text.toString())
                dialog.dismiss()
            } else {
                displayToast(requireContext(), getString(R.string.edit_toast))
            }
        }
        dialogEditOfficeBinding.btnDialogCancel.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun onOfficesUpdated(offices: List<Office>) {
        lifecycleScope.launch(Dispatchers.Main) {
            adapter.submitList(offices)
        }
    }

    private fun setUpBarsAndNavigation() {
        val mainActivity = (requireActivity() as MainActivity)
        mainActivity.setToolbarTitle(R.string.edit_office)
        mainActivity.setToolbarVisibility(true)
        mainActivity.setLogoVisibility(false)
        mainActivity.setProfileIconVisibility(true)
        mainActivity.setBackArrowVisibility(true)

        val navHostFragment = parentFragment as NavHostFragment?
        navHostFragment?.let { navHostFrag ->
            navHostFrag.parentFragment?.let { parentFrag ->
                val navigationFrag = parentFrag as NavigationFragment?
                navigationFrag?.setBottomNavigationBarVisibility(true)
            }
        }
    }

    private fun navigate(officeId: String, officeTitle: String) {
        val action =
            AdminOfficesFragmentDirections.actionAdminOfficesFragmentToAdminEditOfficeFragment(
                officeTitle,
                officeId
            )
        findNavController().navigate(action)
    }
}