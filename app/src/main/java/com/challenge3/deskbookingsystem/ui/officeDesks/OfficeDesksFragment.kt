package com.challenge3.deskbookingsystem.ui.officeDesks

import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.challenge3.deskbookingsystem.MainActivity
import com.challenge3.deskbookingsystem.R
import com.challenge3.deskbookingsystem.databinding.FragmentOfficeDesksBinding
import com.challenge3.deskbookingsystem.repository.model.remote.desk.DeskWithBookings
import com.challenge3.deskbookingsystem.ui.bottomNavigation.NavigationFragment
import com.challenge3.deskbookingsystem.ui.officeDesks.adapter.OfficeDesksAdapter
import com.challenge3.deskbookingsystem.ui.officeDesks.viewmodel.OfficeDesksViewModel
import com.challenge3.deskbookingsystem.utils.*
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.MaterialDatePicker
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import java.text.SimpleDateFormat
import java.util.*

class OfficeDesksFragment : Fragment() {
    private lateinit var binding: FragmentOfficeDesksBinding
    private val adapter: OfficeDesksAdapter by inject()
    private val viewModel: OfficeDesksViewModel by inject()
    private val args: OfficeDesksFragmentArgs by navArgs()
    private var isFix = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentOfficeDesksBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpBarsAndNavigation()

        binding.etStartDate.inputType = InputType.TYPE_NULL
        binding.etEndDate.inputType = InputType.TYPE_NULL
        val startDate = Calendar.getInstance()
        val endDate = Calendar.getInstance()
        setUpFixFlexButtons()

        setTimeOfDayToZero(startDate)
        setTimeOfDayToZero(endDate)

        binding.etStartDate.setText(formatDateToString(startDate))
        binding.etEndDate.setText(formatDateToString(startDate))

        //picker start date
        binding.etStartDate.setOnClickListener {
            val pickerStart = MaterialDatePicker.Builder.datePicker()
                .setTheme(com.google.android.material.R.style.ThemeOverlay_MaterialComponents_MaterialCalendar)
                .setInputMode(MaterialDatePicker.INPUT_MODE_CALENDAR)
                .setCalendarConstraints(
                    CalendarConstraints.Builder().setValidator(DateValidatorWeekdays(null)).build()
                )
                .build()
            pickerStart.addOnPositiveButtonClickListener {
                startDate.timeInMillis = it
                endDate.timeInMillis = it
                //end date should not be before start date
                binding.etEndDate.setText(formatDateToString(endDate))
                updateDeskAvailability(startDate.timeInMillis, endDate.timeInMillis)
                binding.etStartDate.setText(formatDateToString(startDate))
            }
            pickerStart.show(requireActivity().supportFragmentManager, "")
        }

        //picker end date
        binding.etEndDate.setOnClickListener {
            val format = SimpleDateFormat("dd/MM/yyyy", Locale.GERMANY)
            val date = format.parse(binding.etStartDate.text.toString())
            val cal = Calendar.getInstance()
            if (date != null) {
                cal.time = date //date to calendar
            }
            val pickerEnd = MaterialDatePicker.Builder.datePicker()
                .setTheme(com.google.android.material.R.style.ThemeOverlay_MaterialComponents_MaterialCalendar)
                .setInputMode(MaterialDatePicker.INPUT_MODE_CALENDAR)
                .setCalendarConstraints(
                    CalendarConstraints.Builder()
                        .setValidator(
                            DateValidatorWeekdays(
                                calculateEndDate(cal),
                                cal
                            )
                        )
                        .setOpenAt(cal.timeInMillis).build()
                )
                .build()
            pickerEnd.addOnNegativeButtonClickListener() {}
            pickerEnd.addOnPositiveButtonClickListener {
                endDate.timeInMillis = it
                binding.etEndDate.setText(formatDateToString(endDate))
                updateDeskAvailability(startDate.timeInMillis, endDate.timeInMillis)
            }
            pickerEnd.show(requireActivity().supportFragmentManager, "")
        }

        //show desks
        adapter.setClickListener(object : ClickCallback {
            override fun onClick(id: String, title: String) {
                val action =
                    OfficeDesksFragmentDirections.actionOfficeDesksFragmentToDeskDetailFragment(
                        id,
                        startDate,
                        endDate,
                        title,
                        isFix
                    )
                findNavController().navigate(action)
            }
        })

        binding.rvOfficeDesks.adapter = adapter
        binding.rvOfficeDesks.layoutManager = LinearLayoutManager(requireContext())

        viewModel.list.observe(viewLifecycleOwner) { list ->
            onDesksUpdated(list)
        }

        val format = SimpleDateFormat("dd/MM/yyyy", Locale.GERMANY)
        val dateStart = format.parse(binding.etStartDate.text.toString())
        val dateEnd = format.parse(binding.etEndDate.text.toString())
        if (dateStart != null && dateEnd != null) {
            loadDesks(dateStart.time, dateEnd.time)
        }

        viewModel.error.observe(viewLifecycleOwner) { stringId ->
            displayToast(requireContext(), getString(stringId))
        }
    }

    private fun setUpFixFlexButtons() {
        setColorForBtnFix(R.color.blue_munsell)
        setColorForBtnFlex(R.color.dark_blue)

        binding.btnFlex.setOnClickListener {
            isFix = false
            setColorForBtnFix(R.color.blue_munsell)
            setColorForBtnFlex(R.color.dark_blue)
            binding.etStartDate.isEnabled = true
            binding.etEndDate.isEnabled = true
            val format = SimpleDateFormat("dd/MM/yyyy", Locale.GERMANY)
            val dateStart = format.parse(binding.etStartDate.text.toString())
            val dateEnd = format.parse(binding.etEndDate.text.toString())
            if (dateStart != null && dateEnd != null) {
                updateDeskAvailability(dateStart.time, dateEnd.time)
            }
        }

        binding.btnFix.setOnClickListener {
            isFix = true
            setColorForBtnFlex(R.color.blue_munsell)
            setColorForBtnFix(R.color.dark_blue)
            binding.etStartDate.isEnabled = false
            binding.etEndDate.isEnabled = false
            updateDeskAvailability(true)
        }
    }

    private fun updateDeskAvailability(isBtnFixClicked: Boolean) {
        lifecycleScope.launch(Dispatchers.Main) {
            viewModel.updateDeskAvailability(isBtnFixClicked)
        }
    }

    private fun updateDeskAvailability(start: Long, end: Long) {
        lifecycleScope.launch(Dispatchers.Main) {
            viewModel.updateDeskAvailability(start, end)
        }
    }

    private fun loadDesks(start: Long, end: Long) {
        viewModel.loadAllDesksOfOffice(args.officeId, start, end)
    }

    private fun onDesksUpdated(desks: List<DeskWithBookings>) {
        lifecycleScope.launch(Dispatchers.Main) {
            adapter.submitList(desks)
        }
    }

    private fun setUpBarsAndNavigation() {
        val mainActivity = (requireActivity() as MainActivity)
        mainActivity.setToolbarTitle(args.officeName)
        mainActivity.setToolbarVisibility(true)
        mainActivity.setLogoVisibility(false)
        mainActivity.setProfileIconVisibility(true)
        mainActivity.setBackArrowVisibility(true)
        val navHostFragment = parentFragment as NavHostFragment?
        navHostFragment?.let { navHostFrag ->
            navHostFrag.parentFragment?.let { parentFrag ->
                val navigationFrag = parentFrag as NavigationFragment?
                navigationFrag?.setBottomNavigationBarVisibility(true)
            }
        }
    }

    private fun calculateEndDate(startDate: Calendar): Long {
        val date = Calendar.getInstance()
        date.timeInMillis = startDate.timeInMillis
        // last day of possible booking is friday, because it is not allowed to book over the weekend
        date.set(Calendar.DAY_OF_WEEK,6)
        return date.timeInMillis
    }

    private fun setColorForBtnFix(colorId: Int) {
        binding.btnFlex.setTextColor(
            ContextCompat.getColor(
                requireContext(),
                colorId
            )
        )
    }

    private fun setColorForBtnFlex(colorId: Int) {
        binding.btnFix.setTextColor(
            ContextCompat.getColor(
                requireContext(),
                colorId
            )
        )
    }
}