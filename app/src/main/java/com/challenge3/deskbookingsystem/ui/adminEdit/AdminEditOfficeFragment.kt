package com.challenge3.deskbookingsystem.ui.adminEdit

import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.coroutineScope
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.challenge3.deskbookingsystem.MainActivity
import com.challenge3.deskbookingsystem.R
import com.challenge3.deskbookingsystem.databinding.DialogEditOfficeBinding
import com.challenge3.deskbookingsystem.databinding.FragmentAdminEditOfficeBinding
import com.challenge3.deskbookingsystem.repository.model.remote.office.BodyOffice
import com.challenge3.deskbookingsystem.ui.adminEdit.adapter.AdminDeskAdapter
import com.challenge3.deskbookingsystem.ui.adminEdit.viewModel.AdminEditOfficeViewModel
import com.challenge3.deskbookingsystem.ui.bottomNavigation.NavigationFragment
import com.challenge3.deskbookingsystem.utils.ClickCallback
import com.challenge3.deskbookingsystem.utils.displayToast
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class AdminEditOfficeFragment : Fragment() {
    private lateinit var binding: FragmentAdminEditOfficeBinding
    private val args: AdminEditOfficeFragmentArgs by navArgs()
    private val adapter: AdminDeskAdapter by inject()
    private val viewModel: AdminEditOfficeViewModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAdminEditOfficeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpBarsAndNavigation()

        binding.btnNewDesk.setOnClickListener {
            setUpDialogForAddDesk()
        }

        adapter.setClickListener(object : ClickCallback {
            override fun onClick(id: String, title: String) {
                navigate(id, title)
            }
        })

        binding.rvDesks.adapter = adapter
        binding.rvDesks.layoutManager = LinearLayoutManager(requireContext())

        viewModel.office.onEach { office ->
            lifecycleScope.launch(Dispatchers.Main) {
                binding.tvOfficeNameInfo.text = office.name
                binding.tvColumnsInfo.text = office.columns.toString()
                binding.tvRowsInfo.text = office.rows.toString()
                adapter.submitList(office.desks)
            }
        }.launchIn(viewLifecycleOwner.lifecycle.coroutineScope)

        viewModel.desk.onEach { desk ->
            lifecycleScope.launch(Dispatchers.Main) {
                navigate(desk.id, desk.label)
            }
        }.launchIn(viewLifecycleOwner.lifecycle.coroutineScope)

        viewModel.error.observe(viewLifecycleOwner) { stringId ->
            displayToast(requireContext(), getString(stringId))
        }
        viewModel.loadOffice(args.officeId)

        binding.clOfficeName.setOnClickListener {
            setUpDialogForOffice(DialogType.NAME)
        }
        binding.clColumns.setOnClickListener {
            setUpDialogForOffice(DialogType.COLUMNS)
        }
        binding.clRows.setOnClickListener {
            setUpDialogForOffice(DialogType.ROWS)
        }
    }

    private fun setUpDialogForAddDesk() {
        val builder = MaterialAlertDialogBuilder(requireContext())
        val dialogEditOfficeBinding =
            DialogEditOfficeBinding.inflate(LayoutInflater.from(requireContext()))
        builder.setView(dialogEditOfficeBinding.root)
        val dialog = builder.create()

        dialogEditOfficeBinding.tvDialogHeader.text =
            getString(R.string.edit_desk_name)
        dialogEditOfficeBinding.etDialog.hint = getString(R.string.desk_name)

        dialogEditOfficeBinding.btnConfirm.setOnClickListener {
            if (dialogEditOfficeBinding.etDialog.text != null
                && dialogEditOfficeBinding.etDialog.text.toString().isNotEmpty()
            ) {
                viewModel.createDesk(
                    dialogEditOfficeBinding.etDialog.text.toString(),
                    args.officeId
                )
                dialog.dismiss()
            } else {
                displayToast(requireContext(), getString(R.string.edit_toast))
            }
        }
        dialogEditOfficeBinding.btnDialogCancel.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun setUpDialogForOffice(type: DialogType) {
        val builder = MaterialAlertDialogBuilder(requireContext())
        val dialogEditOfficeBinding =
            DialogEditOfficeBinding.inflate(LayoutInflater.from(requireContext()))
        builder.setView(dialogEditOfficeBinding.root)
        val dialog = builder.create()

        val bodyOffice = BodyOffice(
            binding.tvOfficeNameInfo.text.toString(),
            binding.tvColumnsInfo.text.toString().toInt(),
            binding.tvRowsInfo.text.toString().toInt()
        )

        when (type) {
            DialogType.NAME -> {
                dialogEditOfficeBinding.tvDialogHeader.text = getString(R.string.office_name)
                dialogEditOfficeBinding.etDialog.setText(binding.tvOfficeNameInfo.text.toString())
            }
            DialogType.COLUMNS -> {
                dialogEditOfficeBinding.etDialog.inputType = InputType.TYPE_CLASS_NUMBER
                dialogEditOfficeBinding.tvDialogHeader.text = getString(R.string.columns)
                dialogEditOfficeBinding.etDialog.setText(binding.tvColumnsInfo.text.toString())
            }
            DialogType.ROWS -> {
                dialogEditOfficeBinding.etDialog.inputType = InputType.TYPE_CLASS_NUMBER
                dialogEditOfficeBinding.tvDialogHeader.text = getString(R.string.rows)
                dialogEditOfficeBinding.etDialog.setText(binding.tvRowsInfo.text.toString())
            }
        }
        dialog.show()

        dialogEditOfficeBinding.btnConfirm.setOnClickListener {
            when (type) {
                DialogType.NAME -> bodyOffice.name =
                    dialogEditOfficeBinding.etDialog.text.toString()
                DialogType.COLUMNS -> bodyOffice.columns =
                    dialogEditOfficeBinding.etDialog.text.toString().toInt()
                DialogType.ROWS -> bodyOffice.rows =
                    dialogEditOfficeBinding.etDialog.text.toString().toInt()
            }
            viewModel.editOffice(args.officeId, bodyOffice)
            dialog.dismiss()
        }
        dialogEditOfficeBinding.btnDialogCancel.setOnClickListener {
            dialog.dismiss()
        }
    }

    private fun setUpBarsAndNavigation() {
        val mainActivity = (requireActivity() as MainActivity)
        mainActivity.setToolbarTitle(args.officeName)
        mainActivity.setToolbarVisibility(true)
        mainActivity.setLogoVisibility(false)
        mainActivity.setProfileIconVisibility(true)
        mainActivity.setBackArrowVisibility(true)

        val navHostFragment = parentFragment as NavHostFragment?
        navHostFragment?.let { navHostFrag ->
            navHostFrag.parentFragment?.let { parentFrag ->
                val navigationFrag = parentFrag as NavigationFragment?
                navigationFrag?.setBottomNavigationBarVisibility(true)
            }
        }
    }

    private fun navigate(id: String, label: String) {
        val action =
            AdminEditOfficeFragmentDirections.actionAdminEditOfficeFragmentToAdminEditDeskFragment(
                id,
                label
            )
        findNavController().navigate(action)
    }
}

enum class DialogType {
    NAME,
    COLUMNS,
    ROWS
}