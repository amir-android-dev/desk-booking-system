package com.challenge3.deskbookingsystem.ui.adminEdit.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.challenge3.deskbookingsystem.repository.coreRepository.CoreRepository
import com.challenge3.deskbookingsystem.repository.model.remote.desk.BodyDesk
import com.challenge3.deskbookingsystem.repository.model.remote.desk.Desk
import com.challenge3.deskbookingsystem.repository.remoteRepository.RemoteRepositoryImpl
import com.challenge3.deskbookingsystem.utils.Resource
import com.challenge3.deskbookingsystem.utils.messageId
import kotlinx.coroutines.launch

class AdminEditDeskViewModel(private val coreRepository: CoreRepository) : ViewModel() {
    private val _desk: MutableLiveData<Desk> = MutableLiveData()
    val desk: LiveData<Desk>
        get() = _desk

    private val _equipment: MutableLiveData<List<Triple<String, String, Boolean>>> =
        MutableLiveData()
    val equipment: LiveData<List<Triple<String, String, Boolean>>>
        get() = _equipment

    private val _error: MutableLiveData<Int> = MutableLiveData()
    val error: LiveData<Int>
        get() = _error

    fun loadDesk(deskId: String) {
        viewModelScope.launch {
            when (val resource = coreRepository.getDesk(deskId)) {
                is Resource.Error -> {
                    _error.postValue((resource.throwable as RemoteRepositoryImpl.RemoteError).messageId())
                }
                is Resource.Success -> {
                    _desk.postValue(resource.result)
                }
                Resource.SuccessNoBody -> {}
            }
        }
    }

    fun editDesk(deskId: String, bodyDesk: BodyDesk) {
        viewModelScope.launch {
            when (val resource = coreRepository.editDesk(deskId, bodyDesk)) {
                is Resource.Error -> {
                    _error.postValue((resource.throwable as RemoteRepositoryImpl.RemoteError).messageId())
                }
                is Resource.Success -> _desk.postValue(resource.result)
                Resource.SuccessNoBody -> {}
            }
        }
    }

    fun loadEquipment() {
        viewModelScope.launch {
            when (val resource = coreRepository.getPossibleEquipment()) {
                is Resource.Error -> {
                    _error.postValue((resource.throwable as RemoteRepositoryImpl.RemoteError).messageId())
                }
                is Resource.Success -> {
                    //decided to take value for desk equipment entry, because most of the data included the value not the key
                    _equipment.value = resource.result.entrySet().map {
                        Triple(
                            it.key,
                            it.value.asString,
                            _desk.value?.equipment?.contains(it.value.asString) ?: false
                        )
                    }
                }
                Resource.SuccessNoBody -> {}
            }
        }
    }
}