package com.challenge3.deskbookingsystem.ui.login

import com.challenge3.deskbookingsystem.utils.Constants.AUTHORIZATION
import com.challenge3.deskbookingsystem.utils.Constants.BEARER
import okhttp3.*

class AuthTokenInterceptor() : Interceptor {
    companion object {
        var token: String? = null
        var refreshToken: String? = null
        var profileID: String? = null
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        if (token?.isNotEmpty() == true) {
            request = request.newBuilder()
                .addHeader(AUTHORIZATION, "$BEARER $token")
                .build()
        }
        return chain.proceed(request)
    }
}


