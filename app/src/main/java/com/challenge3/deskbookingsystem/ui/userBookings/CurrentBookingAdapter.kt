package com.challenge3.deskbookingsystem.ui.userBookings

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.challenge3.deskbookingsystem.databinding.ItemCurrentBookingsBinding
import com.challenge3.deskbookingsystem.repository.model.db.BookedDeskDbModel
import com.challenge3.deskbookingsystem.utils.displayDateAsString

class CurrentBookingAdapter(private val isPrevious: Boolean = false) :
    ListAdapter<BookedDeskDbModel, CurrentBookingAdapter.MViewHolder>(object :
        DiffUtil.ItemCallback<BookedDeskDbModel>() {
        override fun areItemsTheSame(
            oldItem: BookedDeskDbModel,
            newItem: BookedDeskDbModel
        ) = oldItem.id == newItem.id


        override fun areContentsTheSame(
            oldItem: BookedDeskDbModel,
            newItem: BookedDeskDbModel
        ) = oldItem == newItem

    }) {
    //binding
    private lateinit var binding: ItemCurrentBookingsBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MViewHolder {
        binding =
            ItemCurrentBookingsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MViewHolder()
    }

    override fun onBindViewHolder(holder: MViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    inner class MViewHolder : ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(item: BookedDeskDbModel) {
            if (isPrevious) {
                binding.ivDelete.visibility = View.INVISIBLE
            }
            binding.tvOfficeName.text = "${item.officeName}, ${item.deskLabel}"
            binding.tvStartData.text = displayDateAsString(item.dateStart!!)
            binding.tvEndData.text = displayDateAsString(item.dateEnd!!)
            binding.tvTableNum.text = item.type
            //click listener
            binding.root.setOnClickListener {
                onItemClickListener?.let { it(item) }
            }
        }
    }

    private var onItemClickListener: ((BookedDeskDbModel) -> Unit)? = null
    fun setOnItemClickListener(listener: (BookedDeskDbModel) -> Unit) {
        onItemClickListener = listener
    }

}