package com.challenge3.deskbookingsystem.ui.admin_comments

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.challenge3.deskbookingsystem.databinding.LoadMoreBinding
import com.challenge3.deskbookingsystem.ui.admin_comments.LoadMoreAdapter.MViewHolder

class LoadMoreAdapter(private val retry: () -> Unit) :
    androidx.paging.LoadStateAdapter<MViewHolder>() {
    private lateinit var binding: LoadMoreBinding

    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState): MViewHolder {
        binding = LoadMoreBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MViewHolder(retry)
    }

    override fun onBindViewHolder(holder: MViewHolder, loadState: LoadState) {
        holder.bind(loadState)
    }


    inner class MViewHolder(retry: () -> Unit) : ViewHolder(binding.root) {
        init {
            binding.btnRetry.setOnClickListener { retry() }
        }

        fun bind(state: LoadState) {
            binding.progressBar.isVisible = state is LoadState.Loading
            binding.tvLoadMore.isVisible = state is LoadState.Error
            binding.btnRetry.isVisible = state is LoadState.Error
        }
    }


}