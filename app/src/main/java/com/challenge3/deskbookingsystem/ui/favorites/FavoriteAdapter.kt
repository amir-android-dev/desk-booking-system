package com.challenge3.deskbookingsystem.ui.favorites

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageButton
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.challenge3.deskbookingsystem.R
import com.challenge3.deskbookingsystem.repository.model.remote.favorites.ResponseFavoritesItem

class FavoriteAdapter(private val recyclerViewDeleteClickItem: RecyclerViewDeleteClickItem) :
    ListAdapter<ResponseFavoritesItem, FavoriteAdapter.ViewHolder>(FavoriteDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.favorites_item, parent, false)
        return ViewHolder(itemView, recyclerViewDeleteClickItem)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val favorite: ResponseFavoritesItem = getItem(position)

        holder.tvOffice.text = favorite.desk.office.name
        holder.tvLabel.text = favorite.desk.label
        holder.tvEquipment.text =
            favorite.desk.equipment.toString().replace("[", "").replace("]", "")
    }

    class ViewHolder(itemView: View, recyclerViewInterface: RecyclerViewDeleteClickItem) :
        RecyclerView.ViewHolder(itemView) {

        val tvOffice: TextView = itemView.findViewById(R.id.tvOfficeName)
        val tvLabel: TextView = itemView.findViewById(R.id.tvLabel)
        val tvEquipment: TextView = itemView.findViewById(R.id.tvEquipment)
        val buDelete: AppCompatImageButton = itemView.findViewById(R.id.iv_Delete)

        init {
            buDelete.setOnClickListener {
                val pos = bindingAdapterPosition
                recyclerViewInterface.onClickDeleteItem(pos)
            }

            itemView.setOnClickListener {
                val pos = bindingAdapterPosition
                recyclerViewInterface.onClickItem(pos)
            }
        }
    }

    class FavoriteDiffCallback : DiffUtil.ItemCallback<ResponseFavoritesItem>() {
        override fun areItemsTheSame(
            oldItem: ResponseFavoritesItem,
            newItem: ResponseFavoritesItem
        ): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: ResponseFavoritesItem,
            newItem: ResponseFavoritesItem
        ): Boolean {
            return oldItem == newItem
        }
    }
}