package com.challenge3.deskbookingsystem.ui.register

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.navigation.fragment.findNavController
import com.challenge3.deskbookingsystem.MainActivity
import com.challenge3.deskbookingsystem.R
import com.challenge3.deskbookingsystem.databinding.FragmentRegisterBinding
import com.challenge3.deskbookingsystem.repository.model.remote.register.BodyRegister
import com.challenge3.deskbookingsystem.utils.Department
import com.challenge3.deskbookingsystem.utils.displayToast
import org.koin.android.ext.android.inject

class RegisterFragment : Fragment() {
    //binding
    private lateinit var binding: FragmentRegisterBinding

    //others
    private val viewModel: RegisterViewModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRegisterBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        spinnerSetup()
        setUpBarsAndNavigation()

        //register a user
        binding.btnRegister.setOnClickListener {
            val firstname = binding.etFirstname.text.toString()
            val lastname = binding.etLastname.text.toString()
            val email = binding.etEmail.text.toString()
            val password = binding.etPassword.text.toString()
            val confirmPass = binding.etPasswordReview.text.toString()
            val department = binding.spinnerDepartment
            if (firstname.isEmpty() || lastname.isEmpty() || email.isEmpty() || password.isEmpty() || confirmPass.isEmpty()) {
                displayToast(requireContext(), getString(R.string.fill_all_fields))
            } else if (password != confirmPass) {
                displayToast(
                    requireContext(),
                    getString(R.string.confirm_password_msg)
                )
            } else {
                viewModel.registerUser(
                    BodyRegister(
                        department.selectedItem.toString(),
                        email,
                        firstname,
                        lastname,
                        password
                    )
                )
            }
            loading()
        }
        viewModelObserver()
    }

    private fun setUpBarsAndNavigation() {
        val mainActivity = (requireActivity() as MainActivity)
        mainActivity.setToolbarTitle(R.string.register)
        mainActivity.setToolbarVisibility(true)
        mainActivity.setBackArrowVisibility(true)
        mainActivity.setLogoVisibility(false)
        mainActivity.setProfileIconVisibility(false)
    }

    //if server throw an Error(email exists already) here must be handle
    private fun viewModelObserver() {
        viewModel.responseMsg.observe(viewLifecycleOwner) {
            displayToast(requireContext(), getString(R.string.successfully_registered))
            val action = RegisterFragmentDirections.actionGlobalLoginFragment()
            findNavController().navigate(action)
        }
        viewModel.error.observe(viewLifecycleOwner) {stringId ->
            displayToast(requireContext(), getString(stringId))
        }
    }

    //setup spinner
    private fun spinnerSetup() {
        //array adapter
        val arrayAdapter = ArrayAdapter(
            requireContext(),
            androidx.constraintlayout.widget.R.layout.support_simple_spinner_dropdown_item,
            Department.values()
        )
        binding.spinnerDepartment.adapter = arrayAdapter
    }

    //by sending a request the progressbar will be gone or visible
    private fun loading() {
        viewModel.isRegistered.observe(viewLifecycleOwner) {
            if (it) {
                binding.progressBar.visibility = View.VISIBLE
            } else {
                binding.progressBar.visibility = View.GONE
            }
        }
    }
}