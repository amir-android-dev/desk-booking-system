package com.challenge3.deskbookingsystem.ui.admin_comments


import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.challenge3.deskbookingsystem.databinding.ItemCommentsBinding
import com.challenge3.deskbookingsystem.repository.model.remote.admin_comments.ResponseComments.ResponseCommentsItem
import com.challenge3.deskbookingsystem.utils.displayDateAsString

class CommentsAdapter : PagingDataAdapter<ResponseCommentsItem, CommentsAdapter.MViewHolder>(
    differCallback
) {
    private lateinit var binding: ItemCommentsBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MViewHolder {
        binding = ItemCommentsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MViewHolder()
    }

    override fun onBindViewHolder(holder: MViewHolder, position: Int) {
        holder.bind(getItem(position)!!)
        holder.setIsRecyclable(false)
    }

    inner class MViewHolder : ViewHolder(binding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(comments: ResponseCommentsItem) {
            binding.tvNum.text = comments.desk!!.label
            binding.tvComment.text = comments.comment
            binding.tvDate.text = displayDateAsString(comments.commentedAt!!)
            binding.tvAuthor.text = "-${comments.user!!.firstname} ${comments.user.lastname}"
        }
    }

    companion object {
        val differCallback = object : DiffUtil.ItemCallback<ResponseCommentsItem>() {
            override fun areItemsTheSame(
                oldItem: ResponseCommentsItem,
                newItem: ResponseCommentsItem
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: ResponseCommentsItem,
                newItem: ResponseCommentsItem
            ): Boolean {
                return oldItem == newItem
            }

        }
    }

}