package com.challenge3.deskbookingsystem.ui.officeDesks

import android.os.Parcel
import android.os.Parcelable
import com.challenge3.deskbookingsystem.utils.setTimeOfDayToZero
import com.google.android.material.datepicker.CalendarConstraints
import java.util.*

//used to disable selections in date picker
class DateValidatorWeekdays(
    private val maximumEnd: Long?,
    private val startDate: Calendar = Calendar.getInstance()
) : CalendarConstraints.DateValidator {
    private val today = Calendar.getInstance()

    constructor(parcel: Parcel) : this(null)

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {}

    override fun isValid(date: Long): Boolean {
        if (maximumEnd != null && maximumEnd < date) {
            return false
        }
        today.timeInMillis = date
        val dayOfWeek: Int = today.get(Calendar.DAY_OF_WEEK)
        val nextMonth = Calendar.getInstance()
        setTimeOfDayToZero(startDate)
        nextMonth.add(Calendar.MONTH, 1)
        return dayOfWeek != Calendar.SATURDAY && dayOfWeek != Calendar.SUNDAY && date >= startDate.timeInMillis
                && date < nextMonth.timeInMillis
    }

    companion object CREATOR : Parcelable.Creator<DateValidatorWeekdays> {
        override fun createFromParcel(parcel: Parcel): DateValidatorWeekdays {
            return DateValidatorWeekdays(parcel)
        }

        override fun newArray(size: Int): Array<DateValidatorWeekdays?> {
            return arrayOfNulls(size)
        }
    }
}