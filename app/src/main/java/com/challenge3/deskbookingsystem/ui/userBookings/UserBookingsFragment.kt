package com.challenge3.deskbookingsystem.ui.userBookings

import SwipeToDeleteCallback
import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.coroutineScope
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.challenge3.deskbookingsystem.MainActivity
import com.challenge3.deskbookingsystem.R
import com.challenge3.deskbookingsystem.databinding.DialogCommentBinding
import com.challenge3.deskbookingsystem.databinding.DialogDisplayDeskDetailsBinding
import com.challenge3.deskbookingsystem.databinding.DialogWarningDeleteBinding
import com.challenge3.deskbookingsystem.databinding.FragmentUserBookingsBinding
import com.challenge3.deskbookingsystem.ui.bottomNavigation.NavigationFragment
import com.challenge3.deskbookingsystem.repository.model.remote.comment.BodyComment
import com.challenge3.deskbookingsystem.utils.displayDateAsString
import com.challenge3.deskbookingsystem.utils.displayToast
import com.challenge3.deskbookingsystem.utils.isPast
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

@SuppressLint("NotifyDataSetChanged")
class UserBookingsFragment : Fragment() {
    //binding
    private lateinit var binding: FragmentUserBookingsBinding

    //others
    private val viewModel: UserBookingViewModel by inject()
    private val adapter: CurrentBookingAdapter by inject { parametersOf(false) }
    private val previousAdapter: CurrentBookingAdapter by inject { parametersOf(true) }
    private val fixAdpater: FixAdapter by inject()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentUserBookingsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //observing current and previous desks
        viewModel.desksOfCurrentUser.onEach { desks ->
            //partition
            val bookings = desks.partition { bookedItem -> isPast(bookedItem.dateEnd!!) }
            //current adapter
            binding.rvUserBookingItemsCurrent.layoutManager = LinearLayoutManager(requireContext())
            binding.rvUserBookingItemsCurrent.adapter = adapter
            adapter.submitList(bookings.second)

            //commenting a previous booking is possible but due to the API, only between 00:00 and 00:30
            //previous adapter
            binding.rvUserBookingItemsPrevious.layoutManager = LinearLayoutManager(requireContext())
            binding.rvUserBookingItemsPrevious.adapter = previousAdapter
            previousAdapter.submitList(bookings.first)

            //swipe to delete
            swipeToDelete(binding.rvUserBookingItemsCurrent)
            viewModel.responseMsg.observe(viewLifecycleOwner) {
                displayToast(requireContext(), it)
            }
        }.launchIn(viewLifecycleOwner.lifecycle.coroutineScope)

        //fix items
        viewModel.fixDesksOfCurrentUser.onEach { fixDesks ->
            binding.rvFix.adapter = fixAdpater
            binding.rvFix.layoutManager = LinearLayoutManager(requireContext())
            fixAdpater.submitList(fixDesks)
            //swipe to delete fix
            swipeToDeleteFixDesk(binding.rvFix)
        }.launchIn(viewLifecycleOwner.lifecycle.coroutineScope)

        //observe deskId to delete flex
        lifecycleScope.launchWhenCreated {
            viewModel.deletingDeskId.collectLatest { id ->
                if (id == null) {
                    adapter.notifyDataSetChanged()
                    return@collectLatest
                }
                val newList = adapter.currentList.toMutableList()
                val index = newList.indexOfFirst { it.id == id }
                //avoid exception
                if (index != -1) {
                    newList.removeAt(index)
                    adapter.submitList(newList)
                    displayToast(requireContext(), getString(R.string.succeccfully_delete))
                }
            }
        }

        //observe deskId to delete fix
        lifecycleScope.launchWhenCreated {
            viewModel.deletingFixDeskIdskId.collectLatest { id ->
                if (id == null) {
                    fixAdpater.notifyDataSetChanged()
                    return@collectLatest
                }
                val newList = fixAdpater.currentList.toMutableList()
                val index = newList.indexOfFirst { it.id == id }
                //avoid exception
                if (index != -1) {
                    newList.removeAt(index)
                    fixAdpater.submitList(newList)
                    displayToast(requireContext(), getString(R.string.succeccfully_delete))
                }
            }
        }

        //observe of comment previous desk
        viewModel.responseMsgCommentDesk.observe(viewLifecycleOwner) { stringId ->
            displayToast(requireContext(), getString(stringId))
        }
        //observe errors
        viewModel.error.observe(viewLifecycleOwner) { stringId ->
            displayToast(requireContext(), getString(stringId))
        }

        setupBarsAndNavigation()
        displayDesksOfCurrentUser()
        setupUi()
        //refresh
        binding.swipeCurrent.setOnRefreshListener {
            displayDesksOfCurrentUser()
            binding.swipeCurrent.isRefreshing = false
        }
        //dialogs
        customDialogDesksDetail()
        customDialogComment()

    }

    private fun displayDesksOfCurrentUser() {
        //response of all booked desks of current user
        viewModel.desksOfCurrentUser()
        viewModel.fixDesksOfCurrentUser()
    }

    //delete a desk of current user
    private fun swipeToDelete(recyclerView: RecyclerView) {
        //callback
        val swipeToDeleteCallback = object : SwipeToDeleteCallback(requireContext()) {
            //implement onSwipe
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                //item position
                val itemToDelete = adapter.currentList[viewHolder.absoluteAdapterPosition]
                if (recyclerView.id == binding.rvUserBookingItemsCurrent.id){
                    customDialogToDelete(itemToDelete.id)
                }else{
                    customDialogToDeleteFixDesk(itemToDelete.id)
                }

            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeToDeleteCallback)
        itemTouchHelper.attachToRecyclerView(recyclerView)
    }

    private fun swipeToDeleteFixDesk(recyclerView: RecyclerView) {
        //callback
        val swipeToDeleteCallback = object : SwipeToDeleteCallback(requireContext()) {
            //implement onSwipe
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                //item position
                val itemToDelete = fixAdpater.currentList[viewHolder.absoluteAdapterPosition]
                customDialogToDeleteFixDesk(itemToDelete.id)
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeToDeleteCallback)
        itemTouchHelper.attachToRecyclerView(recyclerView)
    }

    //to show the detail of a reserved current desk
    private fun customDialogDesksDetail() {
        adapter.setOnItemClickListener {
            val detailsDialogBinding = DialogDisplayDeskDetailsBinding.inflate(layoutInflater)
            val dialog = Dialog(requireContext())
            //setting the screen content
            dialog.setContentView(detailsDialogBinding.root)
            detailsDialogBinding.tvOfficeName.text = it.officeName
            detailsDialogBinding.tvStartDate.text = displayDateAsString(it.dateStart!!)
            detailsDialogBinding.tvEndDate.text = displayDateAsString(it.dateEnd!!)
            detailsDialogBinding.tvType.text = it.type
            detailsDialogBinding.tvDeskLabel.text = it.deskLabel

            detailsDialogBinding.tvEquipment.text =
                it.equipments.toString().replace("[", "").replace("]", "")

            //to close dialog
            detailsDialogBinding.btnClose.setOnClickListener {
                dialog.dismiss()
            }
            dialog.setCanceledOnTouchOutside(false)
            dialog.create()
            dialog.show()
            //to match to UI
            matchDialogToUi(dialog)
        }
    }

    //a warning dialog to delete a current reserved desk
    private fun customDialogToDelete(id: String) {
        //binding
        val deleteBinding = DialogWarningDeleteBinding.inflate(layoutInflater)
        val dialog = Dialog(requireContext())
        dialog.setContentView(deleteBinding.root)
        //no
        deleteBinding.btnCancel.setOnClickListener {
            dialog.dismiss()
            adapter.notifyDataSetChanged()
        }
        //yes
        deleteBinding.btnYes.setOnClickListener {
            viewModel.responseDeleteDeskById(id)
            dialog.dismiss()
        }
        //create
        dialog.setCanceledOnTouchOutside(false)
        dialog.create()
        dialog.show()
        //to match to UI
        matchDialogToUi(dialog)
    }

    private fun customDialogToDeleteFixDesk(id: String) {
        //binding
        val deleteBinding = DialogWarningDeleteBinding.inflate(layoutInflater)
        val dialog = Dialog(requireContext())
        dialog.setContentView(deleteBinding.root)
        //no
        deleteBinding.btnCancel.setOnClickListener {
            dialog.dismiss()
            fixAdpater.notifyDataSetChanged()
        }
        //yes
        deleteBinding.btnYes.setOnClickListener {
            viewModel.responseDeleteFixDeskById(id)
            dialog.dismiss()
        }
        //create
        dialog.setCanceledOnTouchOutside(false)
        dialog.create()
        dialog.show()
        //to match to UI
        matchDialogToUi(dialog)
    }

    private fun customDialogComment() {
        previousAdapter.setOnItemClickListener { previousDesk ->
            val commentBinding = DialogCommentBinding.inflate(layoutInflater)
            val dialog = Dialog(requireContext())
            //setting the screen content
            dialog.setContentView(commentBinding.root)
            //title
            commentBinding.tvTitle.text = getString(R.string.comment_desk, previousDesk.deskLabel)
            //to close dialog
            commentBinding.btnCancel.setOnClickListener {
                dialog.dismiss()
            }
            //send comment
            commentBinding.btnYes.setOnClickListener {
                val comment = commentBinding.etComment.text.toString()
                if (comment.isEmpty()) {
                    displayToast(requireContext(), getString(R.string.comment_is_empty))
                } else {
                    viewModel.responseCommentDesk(
                        BodyComment(
                            comment,
                            previousDesk.deskId
                        )
                    )
                    dialog.dismiss()
                }
            }
            dialog.setCanceledOnTouchOutside(false)
            dialog.create()
            dialog.show()
            //to match to UI
            matchDialogToUi(dialog)
        }
    }

    ////to match dialog to UI
    private fun matchDialogToUi(dialog: Dialog) {
        val window: Window? = dialog.window
        window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }

    //changing view by clicking the buttons
    private fun setupUi() {
        //current
        binding.btnCurrent.setTextColor(
            ContextCompat.getColor(
                requireContext(),
                R.color.dark_blue
            )
        )
        binding.btnPrevious.setTextColor(
            ContextCompat.getColor(
                requireContext(),
                R.color.blue_munsell
            )
        )

        binding.btnFix.setTextColor(
            ContextCompat.getColor(
                requireContext(),
                R.color.blue_munsell
            )
        )

        binding.rvUserBookingItemsCurrent.visibility = View.VISIBLE
        binding.rvUserBookingItemsPrevious.visibility = View.GONE
        binding.rvFix.visibility = View.GONE

        binding.btnCurrent.setOnClickListener {
            binding.rvUserBookingItemsCurrent.visibility = View.VISIBLE
            binding.rvUserBookingItemsPrevious.visibility = View.GONE
            binding.rvFix.visibility = View.GONE
            binding.btnCurrent.setTextColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.dark_blue
                )
            )
            binding.btnPrevious.setTextColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.blue_munsell
                )
            )

            binding.btnFix.setTextColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.blue_munsell
                )
            )
        }
        //previous
        binding.btnPrevious.setOnClickListener {
            binding.rvUserBookingItemsCurrent.visibility = View.GONE
            binding.rvUserBookingItemsPrevious.visibility = View.VISIBLE
            binding.rvFix.visibility = View.GONE
            binding.btnCurrent.setTextColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.blue_munsell
                )
            )
            binding.btnPrevious.setTextColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.dark_blue
                )
            )
            binding.btnFix.setTextColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.blue_munsell
                )
            )
        }

        binding.btnFix.setOnClickListener {
            binding.rvUserBookingItemsCurrent.visibility = View.GONE
            binding.rvUserBookingItemsPrevious.visibility = View.GONE
            binding.rvFix.visibility = View.VISIBLE
            binding.btnCurrent.setTextColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.blue_munsell
                )
            )
            binding.btnPrevious.setTextColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.blue_munsell
                )
            )
            binding.btnFix.setTextColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.dark_blue
                )
            )

        }
    }

    private fun setupBarsAndNavigation() {
        (requireActivity() as MainActivity).setUpBarsForTopFragments(R.string.myBookingsTitle)
        val navHostFragment = parentFragment as NavHostFragment?
        navHostFragment?.let { navHostFrag ->
            navHostFrag.parentFragment?.let { parentFrag ->
                val navigationFrag = parentFrag as NavigationFragment?
                navigationFrag?.setBottomNavigationBarVisibility(true)
            }
        }
    }
}

