package com.challenge3.deskbookingsystem.ui.favorites

interface RecyclerViewDeleteClickItem  {
    fun onClickDeleteItem(position: Int)

    fun onClickItem(position: Int)

}