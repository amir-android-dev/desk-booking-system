package com.challenge3.deskbookingsystem.ui.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.challenge3.deskbookingsystem.MainActivity
import com.challenge3.deskbookingsystem.R
import com.challenge3.deskbookingsystem.databinding.FragmentLoginBinding
import com.challenge3.deskbookingsystem.repository.model.remote.login.BodyLogin
import com.challenge3.deskbookingsystem.ui.bottomNavigation.NavigationFragment
import com.challenge3.deskbookingsystem.utils.displaySnackBar
import com.challenge3.deskbookingsystem.utils.displayToast
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.activityViewModel

class LoginFragment : Fragment() {

    private val viewModel: LoginViewModel by activityViewModel()
    private lateinit var binding: FragmentLoginBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // check if the user has a token
        Token().loadToken(requireContext())
        Token().loadRefreshToken(requireContext())
        setUpBarsAndNavigation()

        if (AuthTokenInterceptor.token != null) {
            // get profile id from the user
            viewModel.getProfileId()
        }

        observerLoginUser()

        binding.tvRegister.setOnClickListener {
            val action = LoginFragmentDirections.actionLoginFragmentToRegisterFragment()
            findNavController().navigate(action)
        }

        binding.buLogin.setOnClickListener {
            if (binding.etEmail.text.toString()
                    .isNotBlank() && binding.etPassword.text.toString().isNotBlank()
            ) {
                if (viewModel.isValidEmail(binding.etEmail.text)) {
                    val email = binding.etEmail.text.toString()
                    val password = binding.etPassword.text.toString()
                    viewModel.login(BodyLogin(email, password), requireContext())

                } else {
                    displayToast(requireContext(), getString(R.string.please_enter_a_valid_email))
                }
            } else {
                displayToast(requireContext(), getString(R.string.enter_email_password))
            }
        }
    }

    private fun observerLoginUser() {
        viewModel.error.observe(viewLifecycleOwner) { stringId ->
            displaySnackBar(requireView(), getString(stringId))
        }

        viewModel.isLogged.observe(viewLifecycleOwner) { isLogged ->
            lifecycleScope.launch(Dispatchers.Main) {
                if ((isLogged == true) && (AuthTokenInterceptor.token != null)) {
                    val action = LoginFragmentDirections.actionLoginFragmentToUserBookingsFragment()
                    findNavController().navigate(action)
                }
            }
        }
    }

    private fun setUpBarsAndNavigation() {
        val mainActivity = (requireActivity() as MainActivity)
        mainActivity.setToolbarVisibility(false)
        mainActivity.setLogoVisibility(true)

        val navHostFragment = parentFragment as NavHostFragment?
        navHostFragment?.let { navHostFrag ->
            navHostFrag.parentFragment?.let { parentFrag ->
                val navigationFrag = parentFrag as NavigationFragment?
                navigationFrag?.setBottomNavigationBarVisibility(false)
            }
        }
    }
}





