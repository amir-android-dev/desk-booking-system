package com.challenge3.deskbookingsystem.ui.adminEdit

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.challenge3.deskbookingsystem.MainActivity
import com.challenge3.deskbookingsystem.R
import com.challenge3.deskbookingsystem.databinding.DialogEditOfficeBinding
import com.challenge3.deskbookingsystem.databinding.FragmentEditDeskBinding
import com.challenge3.deskbookingsystem.repository.model.remote.desk.BodyDesk
import com.challenge3.deskbookingsystem.repository.model.remote.desk.Desk
import com.challenge3.deskbookingsystem.ui.adminEdit.adapter.CheckboxCallback
import com.challenge3.deskbookingsystem.ui.adminEdit.adapter.EditEquipmentAdapter
import com.challenge3.deskbookingsystem.ui.adminEdit.viewModel.AdminEditDeskViewModel
import com.challenge3.deskbookingsystem.ui.bottomNavigation.NavigationFragment
import com.challenge3.deskbookingsystem.utils.displayToast
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class AdminEditDeskFragment : Fragment() {
    private lateinit var binding: FragmentEditDeskBinding
    private val args: AdminEditDeskFragmentArgs by navArgs()
    private val adapter: EditEquipmentAdapter by inject()
    private val viewModel: AdminEditDeskViewModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentEditDeskBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpBarsAndNavigation()

        binding.clDeskLabel.setOnClickListener {
            viewModel.desk.value.let { setUpDialogForLabel() }
        }

        adapter.setClickListener(object : CheckboxCallback {
            override fun onClick(equipment: Triple<String, String, Boolean>) {
                viewModel.desk.value.let { desk ->
                    val bodyDesk = BodyDesk(
                        label = desk!!.label,
                        office = desk.office.id,
                        equipment = adapter.currentList.filter {
                            if (it.second == equipment.second) {
                                !equipment.third
                            } else it.third
                        }.map { it.second }
                    )
                    viewModel.editDesk(args.deskId, bodyDesk)
                }
            }
        })

        binding.rvEquipment.adapter = adapter
        binding.rvEquipment.layoutManager = LinearLayoutManager(requireContext())

        viewModel.desk.observe(viewLifecycleOwner) { desk ->
            onDeskUpdated(desk)
            viewModel.loadEquipment()
        }

        viewModel.equipment.observe(viewLifecycleOwner) { equipment ->
            adapter.submitList(equipment)
        }

        viewModel.error.observe(viewLifecycleOwner) { stringId ->
            lifecycleScope.launch(Dispatchers.Main) {
                displayToast(requireContext(), getString(stringId))
            }
        }
        viewModel.loadDesk(args.deskId)
    }

    private fun onDeskUpdated(desk: Desk) {
        lifecycleScope.launch(Dispatchers.Main) {
            binding.tvDeskLabelInfo.text = desk.label
            binding.tvDeskOfficeInfo.text = desk.office.name
        }
    }

    private fun setUpDialogForLabel() {
        val builder = MaterialAlertDialogBuilder(requireContext())
        val dialogEditOfficeBinding =
            DialogEditOfficeBinding.inflate(LayoutInflater.from(requireContext()))
        builder.setView(dialogEditOfficeBinding.root)
        val dialog = builder.create()

        dialogEditOfficeBinding.tvDialogHeader.text = getString(R.string.desk_label)
        dialogEditOfficeBinding.etDialog.setText(binding.tvDeskLabelInfo.text.toString())

        dialog.show()

        dialogEditOfficeBinding.btnConfirm.setOnClickListener {
            val bodyDesk = BodyDesk(
                label = dialogEditOfficeBinding.etDialog.text.toString(),
                viewModel.desk.value!!.office.id,
                viewModel.desk.value!!.equipment
            )

            viewModel.editDesk(args.deskId, bodyDesk)
            dialog.dismiss()
        }
        dialogEditOfficeBinding.btnDialogCancel.setOnClickListener {
            dialog.dismiss()
        }
    }

    private fun setUpBarsAndNavigation() {
        val mainActivity = (requireActivity() as MainActivity)
        mainActivity.setToolbarTitle(args.deskLabel)
        mainActivity.setToolbarVisibility(true)
        mainActivity.setLogoVisibility(false)
        mainActivity.setProfileIconVisibility(true)
        mainActivity.setBackArrowVisibility(true)

        val navHostFragment = parentFragment as NavHostFragment?
        navHostFragment?.let { navHostFrag ->
            navHostFrag.parentFragment?.let { parentFrag ->
                val navigationFrag = parentFrag as NavigationFragment?
                navigationFrag?.setBottomNavigationBarVisibility(true)
            }
        }
    }
}