package com.challenge3.deskbookingsystem.ui.bottomNavigation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import com.challenge3.deskbookingsystem.R
import com.challenge3.deskbookingsystem.databinding.FragmentNavigationBinding
import com.challenge3.deskbookingsystem.ui.login.LoginViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.activityViewModel

class NavigationFragment : Fragment() {
    private val viewModel: LoginViewModel by activityViewModel()
    private lateinit var binding: FragmentNavigationBinding
    private lateinit var navController: NavController
    private lateinit var navigationFragment: NavigationFragment
    private lateinit var navHostFragment: NavHostFragment

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentNavigationBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navigationFragment = this

        navHostFragment =
            navigationFragment.childFragmentManager.findFragmentById(R.id.navHostFragmentContainer) as NavHostFragment
        navController = navHostFragment.findNavController()
        binding.bottomNavigationView.setupWithNavController(navController)

        setUpDrawerContent(binding.bottomNavigationView)

        viewModel.user.observe(viewLifecycleOwner) { user ->
            lifecycleScope.launch(Dispatchers.Main) {
                binding.bottomNavigationView.menu.findItem(R.id.navAdmin).isVisible = user.isAdmin
            }
        }
    }

    fun uncheckAllItemsOfBottomNavigation() {
        binding.bottomNavigationView.menu.setGroupCheckable(0, true, false)
        for (i in 0 until binding.bottomNavigationView.menu.size()) {
            binding.bottomNavigationView.menu.getItem(i).isChecked = false
        }
        binding.bottomNavigationView.menu.setGroupCheckable(0, true, true)
    }

    private fun setUpDrawerContent(bottomNavigationView: BottomNavigationView) {
        bottomNavigationView.setOnItemSelectedListener { item ->
            selectMenuItem(item)
            true
        }
    }

    private fun selectMenuItem(menuItem: MenuItem) {
        when (menuItem.itemId) {
            R.id.navBooking -> {
                navController.navigate(R.id.action_global_officeFragment)
            }
            R.id.navUserBookings -> {
                navController.navigate(R.id.action_global_userBookingsFragment)
            }
            R.id.navAdmin -> {
                navController.navigate(R.id.action_global_adminStartFragment)
            }
            R.id.navFavorites -> {
                navController.navigate(R.id.action_global_favoritesFragment)
            }
        }
    }

    fun setBottomNavigationBarVisibility(isVisible: Boolean) {
        binding.bottomNavigationView.visibility = if (isVisible) View.VISIBLE else View.GONE
    }

}