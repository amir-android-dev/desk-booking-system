package com.challenge3.deskbookingsystem.ui.adminEdit.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.challenge3.deskbookingsystem.databinding.ItemEditEquipmentBinding

interface CheckboxCallback {
    fun onClick(equipment: Triple<String, String, Boolean>)
}

class EditEquipmentAdapter() :
    ListAdapter<Triple<String, String, Boolean>, EditEquipmentAdapter.EquipmentViewHolder>(
        EquipmentDiffUtil()
    ) {
    private var callback: CheckboxCallback? = null

    class EquipmentViewHolder(val binding: ItemEditEquipmentBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(equipment: Triple<String, String, Boolean>, callback: CheckboxCallback?) {
            binding.cbEquipment.text = equipment.second
            binding.cbEquipment.isChecked = equipment.third

            binding.cbEquipment.setOnClickListener {
                callback?.onClick(equipment)
            }
        }
    }

    fun setClickListener(callback: CheckboxCallback) {
        this.callback = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EquipmentViewHolder {
        val binding =
            ItemEditEquipmentBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return EquipmentViewHolder(binding)
    }

    override fun onBindViewHolder(holder: EquipmentViewHolder, position: Int) {
        holder.bind(currentList[position], callback)
    }

    override fun getItemCount(): Int {
        return currentList.size
    }
}

class EquipmentDiffUtil() : DiffUtil.ItemCallback<Triple<String, String, Boolean>>() {
    override fun areItemsTheSame(
        oldItem: Triple<String, String, Boolean>,
        newItem: Triple<String, String, Boolean>
    ): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(
        oldItem: Triple<String, String, Boolean>,
        newItem: Triple<String, String, Boolean>
    ): Boolean {
        return oldItem == newItem
    }
}