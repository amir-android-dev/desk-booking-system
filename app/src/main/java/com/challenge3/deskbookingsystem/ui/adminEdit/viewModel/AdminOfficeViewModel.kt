package com.challenge3.deskbookingsystem.ui.adminEdit.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.challenge3.deskbookingsystem.repository.coreRepository.CoreRepository
import com.challenge3.deskbookingsystem.repository.model.remote.office.Office
import com.challenge3.deskbookingsystem.repository.remoteRepository.RemoteRepositoryImpl
import com.challenge3.deskbookingsystem.utils.Resource
import com.challenge3.deskbookingsystem.utils.messageId
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch

class AdminOfficeViewModel(private val coreRepository: CoreRepository) : ViewModel() {
    private val _list: MutableSharedFlow<List<Office>> =
        MutableSharedFlow(0, 1, BufferOverflow.DROP_LATEST)
    val list: SharedFlow<List<Office>>
        get() = _list

    private val _office: MutableSharedFlow<Office> =
        MutableSharedFlow(0, 1, BufferOverflow.DROP_LATEST)
    val office: SharedFlow<Office>
        get() = _office

    private val _error: MutableLiveData<Int> = MutableLiveData()
    val error: LiveData<Int>
        get() = _error

    fun loadOffices() {
        viewModelScope.launch {
            when (val resource = coreRepository.getAllOffices()) {
                is Resource.Error -> {
                    _error.postValue((resource.throwable as RemoteRepositoryImpl.RemoteError).messageId())
                }
                is Resource.Success -> _list.emit(resource.result)
                Resource.SuccessNoBody -> {}
            }
        }
    }

    fun createOffice(officeName: String) {
        viewModelScope.launch {
            when (val resource = coreRepository.createOffice(officeName)) {
                is Resource.Error -> {
                    _error.postValue((resource.throwable as RemoteRepositoryImpl.RemoteError).messageId())
                }
                is Resource.Success -> _office.emit(resource.result)
                Resource.SuccessNoBody -> {}
            }
        }
    }
}