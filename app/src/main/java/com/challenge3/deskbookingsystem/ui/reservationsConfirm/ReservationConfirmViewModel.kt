package com.challenge3.deskbookingsystem.ui.reservationsConfirm

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.challenge3.deskbookingsystem.R
import com.challenge3.deskbookingsystem.repository.coreRepository.CoreRepository
import com.challenge3.deskbookingsystem.repository.model.remote.reservationsConfirm.BodyApprovedFixDesk
import com.challenge3.deskbookingsystem.repository.model.remote.reservationsConfirm.GetAllFixDesk
import com.challenge3.deskbookingsystem.repository.remoteRepository.RemoteRepositoryImpl
import com.challenge3.deskbookingsystem.utils.Resource
import com.challenge3.deskbookingsystem.utils.messageId
import kotlinx.coroutines.launch

class ReservationConfirmViewModel(private val coreRepo: CoreRepository) : ViewModel() {
    private val _responseMessage = MutableLiveData<Int>()
    val responseMessage: LiveData<Int>
        get() = _responseMessage

    private val _list: MutableLiveData<GetAllFixDesk> = MutableLiveData()
    val list: LiveData<GetAllFixDesk>
        get() = _list

    private val _error: MutableLiveData<Int> = MutableLiveData()
    val error: LiveData<Int>
        get() = _error

    fun getAllFixDeskRequests() {
        viewModelScope.launch {
            when (val resource = coreRepo.getAllFixDeskRequests()) {
                is Resource.Error -> {
                    _error.postValue((resource.throwable as RemoteRepositoryImpl.RemoteError).messageId())
                }
                is Resource.Success -> {
                    val list = resource.result
                    _list.postValue(list)
                }
                Resource.SuccessNoBody -> {}
            }
        }
    }

    fun approveFixDesk(body: BodyApprovedFixDesk) {
        viewModelScope.launch {
            when (val resource = coreRepo.approveFixDeskRequest(body)) {
                is Resource.Error -> {
                    _error.postValue((resource.throwable as RemoteRepositoryImpl.RemoteError).messageId())
                }
                is Resource.Success -> {
                    _responseMessage.postValue(R.string.approved_successful)
                    getAllFixDeskRequests()
                }
                Resource.SuccessNoBody -> {}
            }
        }
    }

    fun deleteFixDeskRequest(id: String) {
        viewModelScope.launch {
            when (val resource = coreRepo.deleteFixDeskRequest(id)) {
                is Resource.Error -> {
                    _error.postValue((resource.throwable as RemoteRepositoryImpl.RemoteError).messageId())
                    getAllFixDeskRequests()
                }
                is Resource.Success -> {
                    getAllFixDeskRequests()
                }
                Resource.SuccessNoBody -> {
                    getAllFixDeskRequests()
                }
            }
        }
    }
}



