package com.challenge3.deskbookingsystem.ui.profile

import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.challenge3.deskbookingsystem.R
import com.challenge3.deskbookingsystem.repository.coreRepository.CoreRepository
import com.challenge3.deskbookingsystem.repository.model.remote.register.BodyRegister
import com.challenge3.deskbookingsystem.repository.model.remote.user.ResponseProfileOfCurrentUser
import com.challenge3.deskbookingsystem.repository.remoteRepository.RemoteRepositoryImpl.RemoteError
import com.challenge3.deskbookingsystem.utils.Constants
import com.challenge3.deskbookingsystem.utils.Resource
import com.challenge3.deskbookingsystem.utils.messageId
import kotlinx.coroutines.launch


class ProfileViewModel(private val coreRepo: CoreRepository) : ViewModel() {

    private val _profile = MutableLiveData<ResponseProfileOfCurrentUser>()
    val profile: LiveData<ResponseProfileOfCurrentUser>
        get() = _profile

    private val _responseMessage = MutableLiveData<Int>()
    val responseMessage: LiveData<Int>
        get() = _responseMessage

    private val _list: MutableLiveData<ResponseProfileOfCurrentUser> = MutableLiveData()
    val list: LiveData<ResponseProfileOfCurrentUser>
        get() = _list

    private val _error: MutableLiveData<Int> = MutableLiveData()
    val error: LiveData<Int>
        get() = _error

    fun loadUserProfile() {
        viewModelScope.launch {
            when (val response = coreRepo.loadProfileUser()) {
                is Resource.Error -> {
                    _error.postValue((response.throwable as RemoteError).messageId())
                }
                is Resource.Success -> {
                    val result = response.result
                    _profile.postValue(result)
                }
                Resource.SuccessNoBody -> {}
            }
        }
    }

    fun updateUserWithID(body: BodyRegister) {
        viewModelScope.launch {
            when (val response = profile.value?.id?.let { coreRepo.updateUserWithID(it, body) }) {
                is Resource.Error -> {
                    _error.postValue((response.throwable as RemoteError).messageId())
                }
                is Resource.Success -> {
                    loadUserProfile()
                    _responseMessage.postValue(R.string.your_profile_is_updated)
                }
                else -> {
                    _error.postValue(R.string.user_not_loaded)
                }
            }
        }
    }

    fun deleteToken(context: Context) {
        val sharedPreferences: SharedPreferences =
            context.applicationContext.getSharedPreferences(
                Constants.SHARE_PREFERENCES,
                Context.MODE_PRIVATE
            )
        sharedPreferences.edit().clear().apply()
    }
}