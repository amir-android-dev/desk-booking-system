package com.challenge3.deskbookingsystem.ui.office

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.challenge3.deskbookingsystem.databinding.ItemOfficeBinding
import com.challenge3.deskbookingsystem.repository.model.remote.office.Office
import com.challenge3.deskbookingsystem.utils.ClickCallback

class OfficeAdapter() : ListAdapter<Office, OfficeAdapter.OfficeViewHolder>(OfficeDiffUtil()) {
    private var clickCallback: ClickCallback? = null

    class OfficeViewHolder(val binding: ItemOfficeBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(office: Office, clickCallback: ClickCallback?) {
            binding.rvButtonOffice.text = office.name
            binding.rvButtonOffice.setOnClickListener {
                clickCallback?.onClick(office.id, office.name)
            }
        }
    }

    fun setClickListener(clickCallback: ClickCallback) {
        this.clickCallback = clickCallback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OfficeViewHolder {
        val binding =
            ItemOfficeBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return OfficeViewHolder(binding)
    }

    override fun onBindViewHolder(holder: OfficeViewHolder, position: Int) {
        holder.bind(currentList[position], clickCallback)
    }

    override fun getItemCount(): Int {
        return currentList.size
    }
}

class OfficeDiffUtil(
) : DiffUtil.ItemCallback<Office>() {
    override fun areItemsTheSame(oldItem: Office, newItem: Office): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Office, newItem: Office): Boolean {
        return oldItem == newItem
    }
}