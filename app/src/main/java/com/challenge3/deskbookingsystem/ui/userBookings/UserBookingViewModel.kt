package com.challenge3.deskbookingsystem.ui.userBookings

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.challenge3.deskbookingsystem.R
import com.challenge3.deskbookingsystem.repository.coreRepository.CoreRepository
import com.challenge3.deskbookingsystem.repository.model.db.BookedDeskDbModel
import com.challenge3.deskbookingsystem.repository.model.db.BookedFixDesksDbModel
import com.challenge3.deskbookingsystem.repository.model.remote.comment.BodyComment
import com.challenge3.deskbookingsystem.repository.remoteRepository.RemoteRepositoryImpl
import com.challenge3.deskbookingsystem.utils.Constants.NO_CONNECTION
import com.challenge3.deskbookingsystem.utils.Constants.SUCCESS
import com.challenge3.deskbookingsystem.utils.Resource
import com.challenge3.deskbookingsystem.utils.messageId
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch

class UserBookingViewModel(private val coreRepository: CoreRepository) : ViewModel() {
    private val _responseMsg = MutableLiveData<String>()
    val responseMsg: LiveData<String>
        get() = _responseMsg
     //current flex
    private val _deletingDeskId: MutableSharedFlow<String?> =
        MutableSharedFlow(0, 1, BufferOverflow.DROP_OLDEST)
    val deletingDeskId: SharedFlow<String?>
        get() = _deletingDeskId

    //current fix
    private val _deletingFixDeskId: MutableSharedFlow<String?> =
        MutableSharedFlow(0, 1, BufferOverflow.DROP_OLDEST)
    val deletingFixDeskIdskId: SharedFlow<String?>
        get() = _deletingFixDeskId

    private val _desksOfCurrentUser: MutableSharedFlow<List<BookedDeskDbModel>> =
        MutableSharedFlow(0, 1, BufferOverflow.DROP_OLDEST)
    val desksOfCurrentUser: SharedFlow<List<BookedDeskDbModel>>
        get() = _desksOfCurrentUser

    private val _fixDesksOfCurrentUser: MutableSharedFlow<List<BookedFixDesksDbModel>> =
        MutableSharedFlow(0, 1, BufferOverflow.DROP_OLDEST)
    val fixDesksOfCurrentUser: SharedFlow<List<BookedFixDesksDbModel>>
        get() = _fixDesksOfCurrentUser

    private val _responseMsgCommentDesk = MutableLiveData<Int>()
    val responseMsgCommentDesk: LiveData<Int>
        get() = _responseMsgCommentDesk

    private val _error: MutableLiveData<Int> = MutableLiveData()
    val error: LiveData<Int>
        get() = _error

    fun desksOfCurrentUser() {
        viewModelScope.launch {
            when (val stringId = coreRepository.loadAndInsertBookingsDesk()) {
                SUCCESS -> {
                    _desksOfCurrentUser.emit(coreRepository.getAllBookedDesks())
                }
                NO_CONNECTION -> {
                    _desksOfCurrentUser.emit(coreRepository.getAllBookedDesks())
                }
                else -> {
                    _error.postValue(stringId)
                }
            }
        }
    }

    fun fixDesksOfCurrentUser() {
        viewModelScope.launch {
            when (val stringId = coreRepository.loadAndInsertFixDesk()) {
                SUCCESS -> {
                    _fixDesksOfCurrentUser.emit(coreRepository.getAllFixDesks())
                }
                NO_CONNECTION -> {
                    _fixDesksOfCurrentUser.emit(coreRepository.getAllFixDesks())
                }
                else -> {
                    _error.postValue(stringId)
                }
            }
        }
    }

    fun responseDeleteDeskById(id: String) {
        viewModelScope.launch {
            when (val resource = coreRepository.deleteBookingWithId(id)) {
                is Resource.Error -> {
                    _error.postValue((resource.throwable as RemoteRepositoryImpl.RemoteError).messageId())
                    _deletingDeskId.emit(null)
                }
                is Resource.Success -> {}
                Resource.SuccessNoBody -> {
                    _deletingDeskId.emit(id)
                }
            }
        }
    }
    fun responseDeleteFixDeskById(id: String) {
        viewModelScope.launch {
            when (val resource = coreRepository.deleteFixDeskById(id)) {
                is Resource.Error -> {
                    _error.postValue((resource.throwable as RemoteRepositoryImpl.RemoteError).messageId())
                    _deletingFixDeskId.emit(null)
                }
                is Resource.Success -> {}
                Resource.SuccessNoBody -> {
                    _deletingFixDeskId.emit(id)
                }
            }
        }
    }

    fun responseCommentDesk(body: BodyComment) {
        viewModelScope.launch {
            when (val resource = coreRepository.postComment(body)) {
                is Resource.Error -> {
                    _error.postValue((resource.throwable as RemoteRepositoryImpl.RemoteError).messageId())
                }
                is Resource.Success -> {
                    _responseMsgCommentDesk.postValue(R.string.comment_success)
                }
                Resource.SuccessNoBody -> {}
            }
        }
    }

}