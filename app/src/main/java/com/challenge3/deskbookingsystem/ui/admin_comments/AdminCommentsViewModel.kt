package com.challenge3.deskbookingsystem.ui.admin_comments


import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import com.challenge3.deskbookingsystem.repository.coreRepository.CoreRepository
import com.challenge3.deskbookingsystem.repository.paging.CommentsPagingSource

class AdminCommentsViewModel(private val coreRepo: CoreRepository) : ViewModel() {

    val commentList = Pager(PagingConfig(10)) {
        CommentsPagingSource(coreRepo)
    }.flow.cachedIn(viewModelScope)

}