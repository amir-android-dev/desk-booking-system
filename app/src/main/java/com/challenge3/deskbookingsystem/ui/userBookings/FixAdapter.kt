package com.challenge3.deskbookingsystem.ui.userBookings


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.challenge3.deskbookingsystem.databinding.ItemFixBookingsBinding
import com.challenge3.deskbookingsystem.repository.model.db.BookedFixDesksDbModel

class FixAdapter :
    ListAdapter<BookedFixDesksDbModel, FixAdapter.MViewHolder>(object :
        DiffUtil.ItemCallback<BookedFixDesksDbModel>() {
        override fun areItemsTheSame(
            oldItem: BookedFixDesksDbModel,
            newItem: BookedFixDesksDbModel
        ) = oldItem.id == newItem.id


        override fun areContentsTheSame(
            oldItem: BookedFixDesksDbModel,
            newItem: BookedFixDesksDbModel
        ) = oldItem == newItem

    }) {
    //binding
    private lateinit var binding: ItemFixBookingsBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MViewHolder {
        binding =
            ItemFixBookingsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MViewHolder()
    }

    override fun onBindViewHolder(holder: MViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    inner class MViewHolder : ViewHolder(binding.root) {

        fun bind(item: BookedFixDesksDbModel) {

            binding.tvOfficeName.text = item.office_name
            binding.tvLabel.text = item.label
            binding.tvStatus.text = item.status
            binding.tvTableNum.text = item.type
        }
    }
}