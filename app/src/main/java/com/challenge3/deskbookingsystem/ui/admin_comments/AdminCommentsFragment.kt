package com.challenge3.deskbookingsystem.ui.admin_comments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.challenge3.deskbookingsystem.MainActivity
import com.challenge3.deskbookingsystem.R
import com.challenge3.deskbookingsystem.databinding.FragmentAdminCommentsBinding
import com.challenge3.deskbookingsystem.ui.bottomNavigation.NavigationFragment
import org.koin.android.ext.android.inject

class AdminCommentsFragment : Fragment() {
    //binding
    private lateinit var binding: FragmentAdminCommentsBinding

    //others
    private val viewModel: AdminCommentsViewModel by inject()
    private val adapter: CommentsAdapter by inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAdminCommentsBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpBarsAndNavigation()
        loadComments()
    }

    private fun loadComments() {
        //collect data
        lifecycleScope.launchWhenCreated {
            viewModel.commentList.collect {
                adapter.submitData(it)
            }
        }
        //loading
        lifecycleScope.launchWhenCreated {
            adapter.loadStateFlow.collect {
                val state = it.refresh
                binding.progressBar.isVisible = state is LoadState.Loading
            }
        }
        //recyclerView
        binding.rvComments.layoutManager = LinearLayoutManager(requireContext())
        binding.rvComments.adapter = adapter
        //swipeRefresh
        binding.swiping.setOnRefreshListener {
            binding.swiping.isRefreshing = false
            adapter.refresh()
        }
        //load more
        binding.rvComments.adapter = adapter.withLoadStateFooter(LoadMoreAdapter {
            adapter.retry()
        })
    }

    private fun setUpBarsAndNavigation() {
        val mainActivity = (requireActivity() as MainActivity)
        mainActivity.setToolbarTitle(R.string.comments)
        mainActivity.setToolbarVisibility(true)
        mainActivity.setLogoVisibility(false)
        mainActivity.setProfileIconVisibility(true)
        mainActivity.setBackArrowVisibility(true)

        val navHostFragment = parentFragment as NavHostFragment?
        navHostFragment?.let { navHostFrag ->
            navHostFrag.parentFragment?.let { parentFrag ->
                val navigationFrag = parentFrag as NavigationFragment?
                navigationFrag?.setBottomNavigationBarVisibility(true)
            }
        }
    }
}