package com.challenge3.deskbookingsystem.ui.reservationsConfirm

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.challenge3.deskbookingsystem.R
import com.challenge3.deskbookingsystem.repository.model.remote.reservationsConfirm.GetAllFixDeskItem
import com.challenge3.deskbookingsystem.ui.favorites.RecyclerViewDeleteClickItem

class ReservationConfirmAdapter(
    private val recyclerViewDeleteClickItem: RecyclerViewDeleteClickItem
) : ListAdapter<GetAllFixDeskItem, ReservationConfirmAdapter.ViewHolder>(DIFF_CALLBACK) {

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<GetAllFixDeskItem>() {
            override fun areItemsTheSame(
                oldItem: GetAllFixDeskItem,
                newItem: GetAllFixDeskItem
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: GetAllFixDeskItem,
                newItem: GetAllFixDeskItem
            ): Boolean {
                return oldItem == newItem
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.reservation_confirm_item, parent, false)
        return ViewHolder(itemView, recyclerViewDeleteClickItem)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val getFixDesk: GetAllFixDeskItem = getItem(position)

        holder.tvOffice.text = getFixDesk.desk.office.name
        holder.tvUserLabel.text = getFixDesk.desk.label
        holder.tvUserFirstName.text = getFixDesk.user.firstname
        holder.tvUserLastName.text = getFixDesk.user.lastname
        holder.tvStatus.text = getFixDesk.status
        if (getFixDesk.status == "approved") {
            holder.buApprove.visibility = View.GONE
        }
    }

    class ViewHolder(itemView: View, recyclerViewInterface: RecyclerViewDeleteClickItem) :
        RecyclerView.ViewHolder(itemView) {
        val tvUserFirstName: TextView = itemView.findViewById(R.id.tvOfficeName)
        val tvUserLastName: TextView = itemView.findViewById(R.id.tvLabel)
        val tvOffice: TextView = itemView.findViewById(R.id.tvEquipment)
        val buDelete: AppCompatImageButton = itemView.findViewById(R.id.bu_Delete)
        val tvUserLabel: AppCompatTextView = itemView.findViewById(R.id.tvUserLabel)
        val buApprove: AppCompatImageButton = itemView.findViewById(R.id.bu_Approve)
        val tvStatus: AppCompatTextView = itemView.findViewById(R.id.tvStatus)

        init {
            buDelete.setOnClickListener {
                val pos = absoluteAdapterPosition
                recyclerViewInterface.onClickDeleteItem(pos)
            }

            buApprove.setOnClickListener {
                val pos = absoluteAdapterPosition
                recyclerViewInterface.onClickItem(pos)
            }
        }
    }
}