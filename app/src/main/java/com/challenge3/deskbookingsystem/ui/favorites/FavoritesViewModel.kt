package com.challenge3.deskbookingsystem.ui.favorites

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.challenge3.deskbookingsystem.R
import com.challenge3.deskbookingsystem.repository.coreRepository.CoreRepository
import com.challenge3.deskbookingsystem.repository.model.remote.favorites.ResponseFavorites
import com.challenge3.deskbookingsystem.ui.login.AuthTokenInterceptor
import com.challenge3.deskbookingsystem.repository.remoteRepository.RemoteRepositoryImpl
import com.challenge3.deskbookingsystem.utils.Resource
import com.challenge3.deskbookingsystem.utils.messageId
import kotlinx.coroutines.launch

class FavoritesViewModel(private val coreRepo: CoreRepository) : ViewModel() {

    private val _deletedMsg = MutableLiveData<Int>()
    val deletedMsg: LiveData<Int>
        get() = _deletedMsg

    private val _list: MutableLiveData<ResponseFavorites> = MutableLiveData()
    val list: LiveData<ResponseFavorites>
        get() = _list

    private val _error: MutableLiveData<Int> = MutableLiveData()
    val error: LiveData<Int>
        get() = _error

    fun getAllFavorites(id: String) {
        viewModelScope.launch {
            when (val resource = coreRepo.getAllFavorites(id)) {
                is Resource.Error -> {
                    _error.postValue((resource.throwable as RemoteRepositoryImpl.RemoteError).messageId())
                }
                is Resource.Success -> {
                    val result = resource.result
                    _list.postValue(result)
                }
                Resource.SuccessNoBody -> {}
            }
        }
    }

    fun deleteFavoriteByID(id: String) {
        viewModelScope.launch {
            when (val resource = coreRepo.deleteFavoriteWithID(id)) {
                is Resource.Error -> {
                    _error.postValue((resource.throwable as RemoteRepositoryImpl.RemoteError).messageId())
                }
                is Resource.Success -> {}
                Resource.SuccessNoBody -> {
                    _deletedMsg.postValue(R.string.success_delete_favorite)
                    getAllFavorites(AuthTokenInterceptor.profileID.toString())
                }
            }
        }
    }
}
