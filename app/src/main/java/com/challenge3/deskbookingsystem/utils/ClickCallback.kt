package com.challenge3.deskbookingsystem.utils

interface ClickCallback {
    fun onClick(id: String, title:String)
}