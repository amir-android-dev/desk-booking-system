package com.challenge3.deskbookingsystem.utils

enum class Department {
    CodingSchool,
    TailoredApps,
    WebundSoehne,
    DarwinsLab,
    DeepDive,
    Diamir
}