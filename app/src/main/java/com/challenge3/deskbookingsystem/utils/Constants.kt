package com.challenge3.deskbookingsystem.utils

object Constants {
    //api
    const val BASE_URL = "https://deskbooking.dev.webundsoehne.com/"
    const val AUTHORIZATION = "Authorization"
    const val BEARER = "Bearer"
    const val SHARE_PREFERENCES = "share_preferences"

    //db
    const val DESK_BOOKING_DATABASE = "desk_booking_database"
    const val BOOKED_DESK_TABLE = "booked_desk_table"
    const val BOOKED_FIX_DESK_TABLE = "booked_fix_desk_table"


    const val SUCCESS = 0
    const val NO_CONNECTION = 1
}