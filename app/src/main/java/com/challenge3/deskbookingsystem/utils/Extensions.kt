package com.challenge3.deskbookingsystem.utils

import android.content.Context
import android.view.View
import android.widget.Toast
import com.challenge3.deskbookingsystem.R
import com.challenge3.deskbookingsystem.repository.remoteRepository.RemoteRepositoryImpl.*
import com.google.android.material.snackbar.Snackbar
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

fun RemoteError.messageId(): Int {
    return when (this) {
        RemoteError.ConflictError -> R.string.error_conflict
        RemoteError.BadRequestError -> R.string.error_bad_request
        RemoteError.NoConnectionError -> R.string.error_no_connection
        RemoteError.NotFoundError -> R.string.error_not_found
        RemoteError.UnauthorizedError -> R.string.error_unauthorized
        RemoteError.UnknownError -> R.string.error_unknown
    }
}

fun displaySnackBar(view: View, msg: String) {
    Snackbar.make(view, msg, Snackbar.LENGTH_LONG).show()
}

fun displayToast(context: Context, msg: String) {
    Toast.makeText(context, msg, Toast.LENGTH_LONG).show()
}
 fun <T> Response<T>.toResource(): Resource<T> {
    return try {
        if (isSuccessful) {
            if (body() != null) {
                Resource.Success(body()!!)
            } else {
                Resource.SuccessNoBody
            }
        } else {
            when (code()) {
                400 -> Resource.Error(RemoteError.BadRequestError)
                401 -> Resource.Error(RemoteError.UnauthorizedError)
                404 -> Resource.Error(RemoteError.NotFoundError)
                409 -> Resource.Error(RemoteError.ConflictError)
                else -> Resource.Error(RemoteError.UnknownError)
            }
        }
    } catch (e: Throwable) {
        Resource.Error(RemoteError.UnknownError)
    }
}

fun formatDateToString(date: Calendar) =
    "${date.get(Calendar.DAY_OF_MONTH)}/" +
            "${date.get(Calendar.MONTH) + 1}/${date.get(Calendar.YEAR)}"


fun setTimeOfDayToZero(endDate: Calendar) {
    endDate.set(
        endDate.get(Calendar.YEAR),
        endDate.get(Calendar.MONTH),
        endDate.get(Calendar.DAY_OF_MONTH),
        0,
        0,
        0
    )
    endDate.set(Calendar.MILLISECOND, 0)
}

fun displayDateAsString(date: String): String {
    val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    return dateFormat.format(dateFormat.parse(date)!!)
}

fun isPast(dateEnd: String): Boolean {
    val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault())
    val date = format.parse(dateEnd)
    val now = Date()

    // Remove the time component from the date
    val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    val dateWithoutTime = dateFormat.parse(dateFormat.format(date!!))
    val nowWithoutTime = dateFormat.parse(dateFormat.format(now))

    return dateWithoutTime!!.before(nowWithoutTime)
}
